//
//  UIKitExtensions.swift
//  Mindfulness
//
//  Created by localadmin on 8/2/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import Pastel


//MARK:- UIViewController Extensions
extension UIViewController  {
    
    func setGradientBackground(view: UIView) {
        
        let pastelView =  PastelView(frame:view.bounds)
        
        pastelView.startPastelPoint =  .bottomLeft
        pastelView.endPastelPoint =  .topRight
        
        //MARK: -  Custom Duration
        
        pastelView.animationDuration = 1.0
        
        //MARK: -  Custom Color
        
        pastelView.setColors([UIColor(red: 222/255, green: 188/255, blue: 196/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 163/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 163/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 0.5373, green: 0.9569, blue: 0.8118, alpha: 0.3),
                              UIColor(red: 138/255, green: 232/255, blue: 214/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 193/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 138/255, green: 232/255, blue: 214/255, alpha: 1.0),
                              UIColor(red: 236/255, green: 193/255, blue: 155/255, alpha: 1.0),
                              UIColor(red: 153/255, green: 255/255, blue: 230/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        
        view.insertSubview(pastelView, at: 0)
        
    }
}


extension UINavigationController {
    
    func pushViewControllerWithPresentAnimation(_ viewController: UIViewController, animated: Bool) {
        
    }
    func pushViewControllerWithPresentAnimation11(viewController: UIViewController, animated: Bool) {
        
    }

}
