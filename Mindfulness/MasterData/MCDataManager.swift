//
//  MCData.swift
//  Mindfulness
//
//  Created by Dinesh Raja on 10/8/15.
//  Copyright © 2015 Mindful Creations. All rights reserved.
//

import Foundation

class MCDataManager: NSObject {
    
    static let shared = MCDataManager()

    var articleObjectsDict = [String: AnyObject]()
    
    func loadData() {
        
        if let path = Bundle.main.path(forResource: "DataObjects", ofType: "plist") {
            
            if let plistDictionary = NSDictionary(contentsOfFile: path) {
                
                for (dictKey, value) in plistDictionary {
                    self.parseMultiLevelArticles(key: dictKey as! String, plistArray: value as! [AnyObject])
                }
            }
            
            print("Data Array is.....\(self.parseMultiLevelArticles)")
        }

        
//        for (dictKey, value) in plistDictionary! {
//
//            if [MCArticleType.Welcome.rawValue, MCArticleType.More.rawValue].contains(dictKey as! String) {
//                self.parseSingleLevelArticles(dictKey as! String, plistArray: value as! [AnyObject])
//            } else {
//                self.parseMultiLevelArticles(dictKey as! String, plistArray: value as! [AnyObject])
//            }
//        }
    }
    
    func parseSingleLevelArticles(key: String, plistArray: [AnyObject]) {
        var singleLevelArticlesArray = [AnyObject]()
        for (_, element) in plistArray.enumerated() {
            let elementDict = element as! Dictionary<String, AnyObject>
            let article = self.parseArticleFromDict(elementDict: elementDict)
            singleLevelArticlesArray.append(article)
        }
        self.articleObjectsDict[key] = singleLevelArticlesArray as AnyObject
    }
    
    func parseMultiLevelArticles(key: String, plistArray: [AnyObject]) {
        var multiLevelArticlesDict = [Int: AnyObject]()
        for (index, element) in plistArray.enumerated() {
            var levelOneArray = [AnyObject]()
            if let elementArray = element as? [AnyObject] {
                for levelOneElement in elementArray {
                    if levelOneElement is NSDictionary {
                        let levelOneElementDict = levelOneElement as! Dictionary<String, AnyObject>
                        let article = self.parseArticleFromDict(elementDict: levelOneElementDict)
                        levelOneArray.append(article)
                    } else {
                        var levelTwoArray = [AnyObject]()
                        let levelTwoElementArray = levelOneElement as! [AnyObject]
                        for levelTwoElement in levelTwoElementArray {
                            let levelTwoElementDict = levelTwoElement as! Dictionary<String, AnyObject>
                            let article = self.parseArticleFromDict(elementDict: levelTwoElementDict)
                            levelTwoArray.append(article)
                        }
                        levelOneArray.append(levelTwoArray as AnyObject)
                    }
                }
            }
            multiLevelArticlesDict[index] = levelOneArray as AnyObject
        }
        self.articleObjectsDict[key] = multiLevelArticlesDict as AnyObject
    }
    
    func parseArticleFromDict(elementDict: [String: AnyObject]) -> MCArticle {
        
        let article = MCArticle()
        
        article.title = elementDict["title"] as? String
        article.titleImagename = elementDict["title_imagename"] as? String
        article.htmlFilename = elementDict["html_filename"] as? String
        article.audioFilename = elementDict["audio_filename"] as? String
        article.audioDuration = elementDict["audio_duration"] as? String
        article.videoFilename = elementDict["video_filename"] as? String
        article.videoDuration = elementDict["video_duration"] as? String
        
//      if let paidArticle = elementDict["paid_article"] as? Bool {
//           article.isPaidArticle = paidArticle
//      }
        if let urlType = elementDict["url_type"] as? Bool {
            article.isURLType = urlType
        }

        return article
    }

    /*func viewTitleForSection(sectionType:MCArticleType.RawValue, subSectionIndex0:Int, subSectionIndex1:Int?) -> String {
        if sectionType == MCArticleType.Experience.rawValue {
            return self.experienceTitles()[subSectionIndex0]
        } else if sectionType == MCArticleType.Knowledge.rawValue {
            if subSectionIndex1 != nil {
                return self.knowledgeSubtitles(subSectionIndex0)[subSectionIndex1!]
            } else {
                return self.knowledgeTitles()[subSectionIndex0]
            }
        } else {
            return ""
        }
    }
    
    func experienceTitles() -> [String]{
        return ["Introduction", "Guided Meditations", "Exercises and tools"]
    }

    func knowledgeTitles() -> [String]{
        return ["Introduction", "Mindfulness", "Mind and Emotions", "Patterns of behaviour", "The art of allowing", "Final words"]
    }
    
    func knowledgeSubtitles(index: Int) -> [String] {
        switch index {
        case 2:
            return ["Your mind", "Thoughts as thoughts", "Feelings as feelings"]
        case 3:
            return ["The little patterns", "Inner resistance", "Resistance traps"]
        default:
            return [String]()
        }
    }*/
}


