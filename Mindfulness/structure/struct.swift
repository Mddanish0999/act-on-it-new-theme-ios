//
//  struct.swift
//  Mindfulness
//
//  Created by localadmin on 6/28/19.
//  Copyright © 2019 Mindful Creations. All rights reserved.
//

import Foundation
import UIKit

struct colorcode {
    static var red: Double = 255
    static var green: Double = 212.0
    static var blue: Double = 1.0
    static var imagesto: String = "yellow"
}
