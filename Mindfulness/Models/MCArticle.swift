//
//  MCArticle.swift
//  Mindfulness
//
//  Created by Dinesh Raja on 10/8/15.
//  Copyright © 2015 Mindful Creations. All rights reserved.
//

import Foundation

class MCArticle: NSObject {
    
    var title: String!
    var htmlFilename: String?
    var titleImagename: String?
    var audioFilename: String?
    var audioDuration: String?
    var videoFilename: String?
    var videoDuration: String?
    var isPaidArticle: Bool = false
    var isURLType: Bool = false
}