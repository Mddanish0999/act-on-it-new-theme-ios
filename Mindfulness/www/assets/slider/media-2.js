// js file for audio / video
function changeImage() {
  var image = document.getElementsByClassName('player')[0];
  if (image.src.match("pause")) {
    image.src = "../../../videos/play-video.png";
  } else {
    image.src = "../../../videos/pause.png";
  }
}

var playing = false;
function action() {
  var audio = document.getElementsByTagName("AUDIO")[0];
  if (playing === true) {
    audio.play();
    playing = false;
  } else {
    audio.pause();
    playing = true;
  }
}
  var clicked = false;
  var counter=1;
function openpopup() {
       if(clicked)
       {
         var video = document.getElementsByTagName("VIDEO")[0];
          if((counter%2)!=0){
          video.pause();
          counter++;
          }
          else{
            video.play();
            counter++;
          }
       }
      else
      {
        var retrivedValue = 'None';
         if (localStorage.getItem('Video URL:', retrivedValue)) {
           document.getElementsByClassName('popup-second')[0].style.display='block';
         }
         else{
           document.getElementsByClassName('popup-first')[0].style.display='block';
         }
         clicked = true;
      }
}

function closepopup(){
  document.getElementsByClassName('popup-first')[0].style.display='none';
  document.getElementsByClassName('popup-second')[0].style.display='none';
}

function onlight() {
  document.getElementsByClassName('white_content')[0].style.display='block';
  document.getElementsByClassName('black_overlay')[0].style.display='block';
}

function fadeOut() {
  document.getElementsByClassName('white_content')[0].style.display='none';
  document.getElementsByClassName('black_overlay')[0].style.display='none';
}
function pauseVideo(){
  var video = document.getElementsByTagName("VIDEO")[0];
  video.pause();
}
function playVideo() {
  // this.paused ? this.play() : this.pause();
  var video = document.getElementsByTagName("VIDEO")[0];
  if (video.paused === true) {
    video.play();
  }
  else{
    video.pause();
  }
}

function playAudio() {
var audio = document.getElementsByTagName("AUDIO")[0];
  audio.play();
}

function pauseAudio() {
var audio = document.getElementsByTagName("AUDIO")[0];
  audio.pause();
}

// implementaion of audio player start
var liveTrackUrl = 'https://www.mindfulcreation.com/Uploads/Audio%20example.mp3'; /* manadatery to provide URL while updating by user. */
var liveTrackUrlTitle = 'Audio%20example'; /* mandatory to provide Title while updating by user. */
var audioUrlToPlay = getAudioUrl(liveTrackUrl);
var receivedTitle = RetrieveVideoUrlFromLocalStorage();

function getAudioUrl(fileUrl) {
  var hrefRawUrl = fileUrl;
  var hrefLastRawUrl = hrefRawUrl.split('/').length-1;
  var hrefSplitedTitle = hrefRawUrl.split('/')[hrefLastRawUrl];
  return hrefSplitedTitle;
}


function SaveToDisk(fileUrl,fileName) {
  var hyperlink = document.createElement('a');
  hyperlink.href = fileUrl;
  hyperlink.target = '_blank';
  hyperlink.download = fileName || fileUrl;

  var mouseEvent = new MouseEvent('click', {
    view: window,
    bubbles: true,
    cancelable: true
  });

  hyperlink.dispatchEvent(mouseEvent);
  (window.URL || window.webkitURL).revokeObjectURL(hyperlink.href);
}

function SaveToLocalStorage(fileUrl,fileName){
  var rawUrl = fileUrl;
  var videoTitle = fileName;
  var lastUrl = rawUrl.split('/').length-1;
  var splitedTitle = rawUrl.split('/')[lastUrl]
  localStorage.setItem('Video URL:', "file:///storage/emulated/0/Download/"+splitedTitle);
  localStorage.setItem('Video Title:', splitedTitle);
  // RetrieveVideoUrlFromLocalStorage();
}

function RetrieveVideoUrlFromLocalStorage() {

  var liveTrackUrl= document.getElementsByTagName("VIDEO")[0].src;
  var liveTrackUrlTitle= "Download File";

  var retrivedValue = 'None';
  if (localStorage.getItem('Video URL:', retrivedValue)) {
    console.log('inside if'+retrivedValue);
    var rawUrl = liveTrackUrl;
    var videoTitle = liveTrackUrlTitle;
    var lastUrl = rawUrl.split('/').length-1;
    var splitedTitle = rawUrl.split('/')[lastUrl]
    var localvideo=document.getElementsByTagName("VIDEO")[0];
    // play as per platform
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      localvideo.src="/Video/"+splitedTitle;
      // alert('ios'+splitedTitle);
    }
    if (/android/i.test(userAgent)) {
      localvideo.src="file:///storage/emulated/0/Download/"+splitedTitle;
      // alert('android'+splitedTitle);
    }
    if (/windows phone/i.test(userAgent)) {
       localvideo.src="file:///storage/emulated/0/Download/"+splitedTitle;
      // alert('windows');
    }
    var retrivedValue = localStorage.getItem('Video URL:', "file:///storage/emulated/0/Download/"+splitedTitle);
  } else {
    console.log('inside else'+retrivedValue);
    SaveToDisk(liveTrackUrl,liveTrackUrlTitle);
    SaveToLocalStorage(liveTrackUrl,liveTrackUrlTitle);
    document.getElementById('audio-container').className += ' disabled'
    document.getElementById('hide-me').className = 'warning'
    document.getElementById('hide-warning').className = 'warning'
  }

  var allTextLines = retrivedValue.split(/\r/); /* to get line of file */
  var line = allTextLines[0].split(',');
  var fileUrl = line[0].split(',');
  var trackName = fileUrl[0].split("Download/")[1].split('.')[0];
  return trackName;
}
