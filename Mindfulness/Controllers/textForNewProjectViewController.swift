//
//  textForNewProjectViewController.swift
//  Mindfulness
//
//  Created by localadmin on 8/31/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import CoreData
import LIHImageSlider

class textForNewProjectViewController: UIViewController ,LIHSliderDelegate {
    func itemPressedAtIndex(index: Int) {
        print("index \(index) is pressed")
    }
    
    public var transitionInterval: Double = 25.0
    public var showPageIndicator: Bool = false
    @IBAction func changeState(_ sender: UIButton) {
     
    
pause()
    }
    
    @IBOutlet weak var slider1Container: UIView!
    fileprivate var sliderVc1: LIHSliderViewController!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
    }
      override func viewDidLoad() {
        super.viewDidLoad()
       
   
        
        
        //Image slider configurations
        var images: [UIImage] = [UIImage(named: "A1.jpg")!,UIImage(named: "wall.jpg")!,
      UIImage(named: "bench.jpg")!]
        _ = images.takeRandomly(numberOfElements: 3)

        let slider1: LIHSlider = LIHSlider(images: images)
        slider1.sliderDescriptions = [""]
        self.sliderVc1  = LIHSliderViewController(slider: slider1)
    
        sliderVc1.delegate = self
        self.addChildViewController(self.sliderVc1)
        self.view.addSubview(self.sliderVc1.view)
        self.sliderVc1.didMove(toParentViewController: self)
   
      }
    
    override func viewDidLayoutSubviews() {
        
        self.sliderVc1!.view.frame = self.slider1Container.frame
    }
   
    
      override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    

}
extension Array {
    mutating func takeRandomly(numberOfElements n: Int) -> ArraySlice<Element> {
        assert(n <= self.count)
        for i in stride(from: self.count - 1, to: self.count - n - 1, by: -1) {
            let randomIndex = Int(arc4random_uniform(UInt32(i + 1)))
            self.swapAt(i, randomIndex)
        }
        return self.suffix(n)
    }
}
extension CALayer {

    func pause() {
        let pausedTime: CFTimeInterval = self.convertTime(CACurrentMediaTime(), from: nil)
        self.speed = 0.0
        self.timeOffset = pausedTime
        
    }
    func resume() {
        let pausedTime: CFTimeInterval = self.timeOffset
        self.speed = 4.0
        self.timeOffset = 3.0
        self.beginTime = 0.0
        let timeSincePause: CFTimeInterval = self.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        self.beginTime = timeSincePause
    }
}
