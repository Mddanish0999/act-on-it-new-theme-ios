//
//  afterQuoteViewController.swift
//  Mindfulness
//
//  Created by localadmin on 8/1/19.
//  Copyright © 2019 Mindful Creations. All rights reserved.
//

import UIKit

class afterQuoteViewController: UIViewController {
    
    var timer: Timer!
    var backgroundColours = [UIColor()]
    var backgroundLoop = 0
    
    var timerTest: Timer? = nil {
        willSet {
            timerTest?.invalidate()
        }
    }
    
    @IBOutlet weak var nameLAbel: UILabel!
    
    var colorCodes = colorcode()
    
    @objc func changeBackround() {
        let colors = [
            UIColor(red: 235/255, green: 212/255, blue: 38/255, alpha: 1.0),
             
            UIColor(red: 0/255, green: 0/255, blue: 255/255 , alpha: 1.0),
            UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0),
            UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0),
            UIColor(red: 255/255, green: 59/255, blue: 28/255, alpha: 1.0),
            UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)
        ]

        UIView.animate(withDuration: 0.8) {
            self.view.backgroundColor = colors[self.backgroundLoop]
        }
        
        if self.view.backgroundColor == UIColor(red: 235/255, green: 212/255, blue: 38/255, alpha: 1.0) {
            self.backgroundLoop = self.backgroundLoop + 1
            self.logoImage.image = UIImage(named: "yellow")
            UIView.animate(withDuration: 0.8) {
                self.nameLAbel.textColor = .white
                self.nameLAbel.text = "Notice."
            }
        }
    
        if self.view.backgroundColor == UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0) {
            self.backgroundLoop = self.backgroundLoop + 1
            self.logoImage.image = UIImage(named: "")
            UIView.animate(withDuration: 0.8) {
                self.nameLAbel.textColor = .white
                self.nameLAbel.text = "Notice."
            }
        }
        
        if self.view.backgroundColor == UIColor(red: 0/255, green: 0/255, blue: 255/255 , alpha: 1.0) {
            UIView.animate(withDuration: 0.8) {
                self.logoImage.image = UIImage(named: "blue")
                self.backgroundLoop = self.backgroundLoop + 1
                self.nameLAbel.textColor = .white
                self.nameLAbel.text = "Allow."
            }
        }

        if self.view.backgroundColor == UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0) {
            UIView.animate(withDuration: 0.8) {
                self.logoImage.image = UIImage(named: "pink")
                self.backgroundLoop = self.backgroundLoop + 1
                self.nameLAbel.textColor = .white
                self.nameLAbel.text = "Explore."
            }
        }
    
        if self.view.backgroundColor == UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0) {
            UIView.animate(withDuration: 0.8) {
                self.logoImage.image = UIImage(named: "lightgreen")
                self.backgroundLoop = self.backgroundLoop + 1
                self.nameLAbel.textColor = .white
                self.nameLAbel.text = "Breathe."
            }
        }
    
        if self.view.backgroundColor == UIColor(red: 255/255, green: 59/255, blue: 28/255, alpha: 1.0) {
            UIView.animate(withDuration: 0.8) {
                self.logoImage.image = UIImage(named: "red")
                self.backgroundLoop = self.backgroundLoop + 1
                self.nameLAbel.textColor = .white
                self.nameLAbel.text = "Connect."
            }
        }

        if self.view.backgroundColor == UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0) {
            UIView.animate(withDuration: 0.8) {
                self.logoImage.image = UIImage(named: "lightblue")
                self.nameLAbel.text = "ACT"
                self.nameLAbel.textColor = UIColor.black
                self.view.backgroundColor = UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)
            }
        
            timerTest!.invalidate()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                let mainMenuController = self.storyboard?.instantiateViewController(withIdentifier: "graphViewController") as! graphViewController
                self.navigationController?.pushViewController(mainMenuController, animated: false)
            })
       }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        timerTest = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(changeBackround), userInfo: nil, repeats: true)
    }
    
    @IBOutlet weak var logoImage: UIImageView!

    }

extension UIColor {
    static func randomColor() -> UIColor {
        let red = CGFloat(drand48())
        let green = CGFloat(drand48())
        let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
