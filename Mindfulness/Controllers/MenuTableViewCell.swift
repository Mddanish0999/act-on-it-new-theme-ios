//
//  MenuTableViewCell.swift
//  Mindfulness
//
//  Created by localadmin on 7/19/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import Pastel
import Foundation
import KWTextStyleLabel

class MenuTableViewCell: UITableViewCell {
 
    @IBOutlet weak var mainSubView: UIView!
    
    @IBOutlet weak var bottomBorderView: UIView!
    @IBOutlet weak var leftBorderView: UIView!
   
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var subDetailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        menuLabel.textColor = .black
    }
    
    func setupCell(menuDict: [String: Any], indexPath: IndexPath) {
        if let value = menuDict["Name"] as? String {
            menuLabel.text = value
        }
 
        if let value = menuDict["detailsText"] as? String {
            subDetailsLabel.text = value
        } else {
           subDetailsLabel.text = ""
        }
    }
}
