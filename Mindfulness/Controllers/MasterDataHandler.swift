//
//  MasterDataHandler.swift
//  ProjectReadiness
//

import Foundation

class MasterDataHandler {
    
    static let sharedInstance = MasterDataHandler()
    
    var masterDataDict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "MindfulnessMasterData", ofType: "plist")!)
    
    func getMainMenues() -> [[String: Any]]? {
        if let dict = masterDataDict {
            if let menuesArray = dict["MainMenus"] as? [[String: Any]] {
                return menuesArray
            }
        }
        return nil
    }
    
    func getHeaderMenues() -> [[String: Any]]? {
        if let dict = masterDataDict {
            if let HeaderArray = dict["RightMenu"] as? [[String: Any]] {
                return HeaderArray
            }
        }
        return nil
    }
}
