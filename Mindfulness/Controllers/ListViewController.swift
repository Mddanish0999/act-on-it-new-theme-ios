//
//  ListViewController.swift
//  Mindfulness
//
//  Created by localadmin on 8/4/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import Foundation
import Pastel

class ListViewController: UIViewController {
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var listTableViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var listTableViewTrailingConstraint: NSLayoutConstraint!
   
    @IBOutlet weak var breadcrumbView: UIView!
    @IBOutlet weak var breadcrumbViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightHeaderButton: UIButton!
    @IBOutlet weak var rightHeaderButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightHeaderButtonTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lightblue: UIButton!
    @IBOutlet weak var red: UIButton!
    @IBOutlet weak var blue: UIButton!
    @IBOutlet weak var pink: UIButton!
    @IBOutlet weak var green: UIButton!
    @IBOutlet weak var yellow: UIButton!
    
    var breadcrumbTitlesArray = [String]()
    var backgroundcolor: UIColor?
    
    var leadingConstraintConstant: CGFloat = 0.0
    var topConstraintConstant: CGFloat = 0.0
  
    var colorCodes = colorcode()
    static var breadcrumptittle = [String]()
    
    static var red = 0.0
    static var blue = 0.0
    static var green = 0.0
    static var backcolor = " "
    
    @IBAction func moreMenuButton(_ sender: UIButton) {
        let modalViewController = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "MoreViewController") as? MoreViewController
        let navContr = UINavigationController(rootViewController: modalViewController!)
        navContr.modalPresentationStyle = .custom  // << Mark this update
        navContr.modalTransitionStyle = .crossDissolve
    
        present(navContr, animated: false, completion: nil)
    }
    //MARK:- Data properties
    var menuDict: [String: Any]!
    var leftHeaderTitle: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paintSafeAreaBottomInset(withColor: .white)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ListViewController.imageTapped(gesture:)))
        logoImage.addGestureRecognizer(tapGesture)
        logoImage.isUserInteractionEnabled = true
        
        listTableViewLeadingConstraint.constant = self.view.bounds.width
        listTableViewTrailingConstraint.constant = -self.view.bounds.width
        self.setupView()
        
        ListViewController.breadcrumptittle = breadcrumbTitlesArray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ListViewController.red = colorcode.red/255
        ListViewController.green = colorcode.green/255
        ListViewController.blue = colorcode.blue/255
        self.view.backgroundColor = UIColor.clear
        ListViewController.backcolor = colorcode.imagesto
    
        DispatchQueue.main.async {
            self.animateListView()
                self.view.backgroundColor = UIColor(red: CGFloat(ListViewController.red), green:  CGFloat(ListViewController.green), blue: CGFloat(ListViewController.blue), alpha: 1.0)
                self.logoImage.image = UIImage(named: ListViewController.backcolor)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        listTableViewLeadingConstraint.constant = -self.view.bounds.width
        listTableViewTrailingConstraint.constant = self.view.bounds.width

        self.view.backgroundColor = UIColor.clear
    }
    
    @IBAction func changecolor(_ sender: UIButton) {
        switch sender {
        case yellow:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 24/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 24
                colorcode.green = 212
                colorcode.imagesto = "yellow"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case blue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1.0)
                colorcode.red = 0
                colorcode.blue = 255
                colorcode.green = 0
                colorcode.imagesto = "blue"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
        
        case pink:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 245
                colorcode.green = 177
                colorcode.imagesto = "pink"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case green:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0)
                colorcode.red = 156
                colorcode.blue = 72
                colorcode.green = 222
                colorcode.imagesto = "lightgreen"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case red:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
                colorcode.red = 225
                colorcode.blue = 0
                colorcode.green = 0
                colorcode.imagesto = "red"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case lightblue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)
                colorcode.red = 150
                colorcode.blue = 255
                colorcode.green = 239
                colorcode.imagesto = "lightblue"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        default:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 240/255, blue: 255/255, alpha: 1.0)
                colorcode.imagesto = "lightblue"
            }
            return
        }
    }

    func animateListView() {
        listTableViewLeadingConstraint.constant = 50
        listTableViewTrailingConstraint.constant = 50
        rightHeaderButtonLeadingConstraint.constant = leadingConstraintConstant
        rightHeaderButtonTopConstraint.constant = topConstraintConstant
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func setupView() {
        rightHeaderButtonLeadingConstraint.constant = self.view.bounds.width
        
        leadingConstraintConstant = 0.0
        topConstraintConstant = 0.0
        
        var heightConstraintConstant: CGFloat = 30.0
        let breadcrumbViewWidth = self.view.frame.width - 80
        
        for view in self.breadcrumbView.subviews {
            if view != rightHeaderButton {
                view.removeFromSuperview()
            }
        }
        
        var tag = 0
        
        for title in breadcrumbTitlesArray {
            let button = UIButton()
            button.tag = tag
            tag = tag + 1
            button.titleLabel?.font = rightHeaderButton.titleLabel?.font
            button.addTarget(self, action: #selector(self.breadcrumbButtonAction(_:)), for: .touchUpInside)
            
            button.setTitleColor(UIColor.black, for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle(" > \(title)", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            
            button.sizeToFit()
            
            breadcrumbView.addSubview(button)
            if leadingConstraintConstant + button.frame.width > breadcrumbViewWidth {
                leadingConstraintConstant = 0
                topConstraintConstant = topConstraintConstant + button.frame.height
                heightConstraintConstant = topConstraintConstant + button.frame.height
            }
            
            button.topAnchor.constraint(equalTo: breadcrumbView.topAnchor, constant: topConstraintConstant).isActive = true
            button.leadingAnchor.constraint(equalTo: breadcrumbView.leadingAnchor, constant: leadingConstraintConstant).isActive = true
            
            leadingConstraintConstant = leadingConstraintConstant + button.frame.width
        }
        
        if let value = menuDict["Name"] as? String {
            ListViewController.breadcrumptittle =  breadcrumbTitlesArray
            ListViewController.breadcrumptittle.append(value)
    
            rightHeaderButton.setTitle (" > \(value)", for: .normal)
            rightHeaderButton.sizeToFit()
        
            if leadingConstraintConstant + rightHeaderButton.frame.width > breadcrumbViewWidth {
                leadingConstraintConstant = 0
                topConstraintConstant = topConstraintConstant + rightHeaderButton.frame.height
                heightConstraintConstant = topConstraintConstant + rightHeaderButton.frame.height
            }
        } else {
            rightHeaderButton.isHidden = true
        }
        
        rightHeaderButton.tag = tag
        breadcrumbViewHeightConstraint.constant = heightConstraintConstant
    }
    
    @IBAction func breadcrumbButtonAction(_ sender: UIButton) {
        if sender == rightHeaderButton {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[sender.tag + 3], animated: false)
            }
        } else {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[sender.tag + 3], animated: false)
            }
        }
    }
    
    // if logo touched then it goes to  home menu
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if (gesture.view as? UIImageView) != nil {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[2], animated: false)
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if let subItems = menuDict["subItems"] as? [[String: Any]] {
            count = subItems.count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
        if let subItems = menuDict["subItems"] as? [[String: Any]]  {
            cell.setupCell(menuDict: subItems[indexPath.row], indexPath: indexPath)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let subItems = menuDict["subItems"] as? [[String: Any]]  {
            if let mediaType = subItems[indexPath.row]["mediaType"] as? String {
           
                if mediaType == "html" {
                    if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCWebViewController") as? MCWebViewController {
                        
                        if let value = menuDict["Name"] as? String {
                            ListViewController.breadcrumptittle = breadcrumbTitlesArray
                            controller.breadcrumbTitlesArray = breadcrumbTitlesArray
                            controller.breadcrumbTitlesArray.append(value)
                        }
                        
                        if let htmlName = subItems[indexPath.row]["htmlName"] as? String {
                            if let urlname = subItems[indexPath.row]["DownloadLink"] as? String {
                                controller.urltouse = urlname
                            }
                        
                            controller.htmlName = htmlName
                            if let name = subItems[indexPath.row]["Name"] as? String {
                                controller.breadcrumbTitlesArray.append(name)
                            }
                            self.navigationController?.pushViewController(controller, animated: false)
                        }
                    }
                } else if mediaType == "video" {
                    if let  controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "videoViewController") as? videoViewController {
                        if let htmlName = subItems[indexPath.row]["htmlName"] as? String {
                            controller.htmlName = htmlName
                            
                            self.navigationController?.pushViewController(controller, animated: false)
                        }
                    }
                } else if mediaType == "audio" {
                    //TODO Audio work
                }
            } else {
                if let menuList = menuDict["subItems"] as? [[String: Any]] {
                    let menuDict = menuList[indexPath.row]
                    if let subItems = menuDict["subItems"] {
                        
                        if let type =  menuDict["subItemsType"] as? String {
                            if type == "listMenu" {
                                if let  controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController {
                                    controller.menuDict = menuList[indexPath.row]
                                    
                                    if let value = self.menuDict["Name"] as? String {
                                        controller.breadcrumbTitlesArray =  self.breadcrumbTitlesArray
                                        controller.breadcrumbTitlesArray.append(value)
                                        ListViewController.breadcrumptittle.append(value)
                                    }
                                    
                                    self.navigationController?.pushViewController(controller, animated: false)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension ListViewController {
    func presentModal() {
        let modalController = MoreViewController()
        modalController.modalPresentationStyle = .overCurrentContext
        present(modalController, animated: true, completion: nil)
    }
}
