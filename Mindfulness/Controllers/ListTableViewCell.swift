//
//  ListTableViewCell.swift
//  Mindfulness
//
//  Created by localadmin on 8/4/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import Pastel

class ListTableViewCell: UITableViewCell {

    //@IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.numberOfLines = 0
        nameLabel.textColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(menuDict: [String: Any], indexPath: IndexPath) {
//        if let value = menuDict["Day"] as? String {
//            dayLabel.text = value
//        } else {
//            dayLabel.text = ""
//        }

        if let value = menuDict["Name"] as? String {
            nameLabel.text = value
            nameLabel.sizeToFit()
        } else {
            nameLabel.text = ""
            nameLabel.sizeToFit()
        }

        if let value = menuDict["detailsText"] as? String {
            detailLabel.text = value
            detailLabel.sizeToFit()
        } else {
            detailLabel.text = ""
            detailLabel.sizeToFit()
        }
    }
}
