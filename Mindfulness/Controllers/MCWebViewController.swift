//
//  MCWebViewController.swift
//  Mindfulness
//
//  Created by localadmin on 8/21/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import WebKit
import AVKit
import SafariServices
import Alamofire
import AVFoundation
import Pastel

class Connectivity {
    class var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class MCWebViewController: UIViewController, AVPictureInPictureControllerDelegate, AVPlayerViewControllerDelegate, NetworkSpeedProviderDelegate {
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var wkWebView: WKWebView!
    
    @IBOutlet weak var trailingLeadingContraint: NSLayoutConstraint!
    @IBOutlet weak var webviewLeadinContraint: NSLayoutConstraint!
    
    @IBOutlet weak var lightblue: UIButton!
    @IBOutlet weak var red: UIButton!
    @IBOutlet weak var blue: UIButton!
    @IBOutlet weak var pink: UIButton!
    @IBOutlet weak var green: UIButton!
    @IBOutlet weak var yellow: UIButton!
    
    @IBOutlet weak var breadcrumbView: UIView!
    @IBOutlet weak var rightHeaderButton: UIButton!
    
    @IBOutlet weak var breadcrumbViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightHeaderButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightHeaderButtonTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var breadcrumbViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var breadcrumbViewTrailingConstraint: NSLayoutConstraint!

    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var backwardButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var playbackSlider: UISlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var controlView: UIView!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var playerCancelButton: UIButton!
    @IBOutlet weak var playerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var playbackSliderWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var downloadInformView: UIView!
    @IBOutlet weak var downloadInformLabel: UILabel!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var alertView2: UIView!
    @IBOutlet weak var cancelImage: UIImageView!
    @IBOutlet weak var retryButton: UIButton!
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var alertViewCloseButton: UIButton!
    @IBOutlet weak var downloadProgressLabel: UILabel!
    
    var leadingConstraintConstant: CGFloat = 0.0
    var topConstraintConstant: CGFloat = 0.0
    var breadcrumbTitlesArray = [String]()
    
    var breadcrumbBtn = [UIButton]()
    
    static var red = 0.0
    static var blue = 0.0
    static var green = 0.0
    static var backcolor = " "
    var urltouse: String = ""
    
    var urlString: String!
    var download: Download!
    var htmlName: String!
    var request: URLRequest!
    var url: String!
    var player: AVPlayer!
    var playerViewController = AVPlayerViewController()
    var timeObserverToken: AnyObject?
    var observer: NSKeyValueObservation?
    var originalFrame = CGRect.zero
    var playerLayer: AVPlayerLayer!
    var isPlaying = false
    var senderTag: Int!
    
    let test = NetworkSpeedTest()
    
    var informLabelTimer: Timer!
    
    @IBAction func moreMenuButton(_ sender: UIButton) {
        if let MoreViewController = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "MoreViewController") as? MoreViewController {
            self.navigationController?.pushViewController(MoreViewController, animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pink.backgroundColor = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
        blurView.alpha = 0
        downloadInformView.alpha = 0
        setupView()
        
        self.wkWebView.isOpaque = false
        MCWebViewController.backcolor = colorcode.imagesto
        DispatchQueue.main.async {
            self.view.backgroundColor = UIColor(red: CGFloat(MCWebViewController.red), green: CGFloat(MCWebViewController.green), blue: CGFloat(MCWebViewController.blue), alpha: 1.0)
            self.wkWebView.backgroundColor = UIColor(red: CGFloat(MCWebViewController.red), green: CGFloat(MCWebViewController.green), blue: CGFloat(MCWebViewController.blue), alpha: 1.0)
            self.logoImage.image = UIImage(named: MCWebViewController.backcolor)
        }
        
        wkWebView.navigationDelegate = self
        self.refreshArticle()
        
        test.delegate = self
        test.networkSpeedTestStop()
        
        paintSafeAreaBottomInset(withColor: .white)
        
        MCWebViewController.red = colorcode.red/255
        MCWebViewController.green = colorcode.green/255
        MCWebViewController.blue = colorcode.blue/255
        self.view.backgroundColor = UIColor.clear
        
        playerViewHeightConstraint.constant = (view.frame.size.height)/2
        playbackSliderWidthConstraint.constant = (view.frame.size.width)/2
        
        self.downloadInformView.layer.borderWidth = 4
        self.downloadInformView.layer.borderColor = UIColor.black.cgColor
        self.downloadInformView.backgroundColor = .lightGray
        
        downloadInformLabel.font = UIFont(name: "BureauGroteskFBOneFive", size: 18)
        downloadInformLabel.text = "Your file is downloading and buffering..."
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MCWebViewController.imageTapped(gesture:)))
        logoImage.addGestureRecognizer(tapGesture)
        cancelImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        logoImage.isUserInteractionEnabled = true
        
        webviewLeadinContraint.constant = self.view.bounds.width
        trailingLeadingContraint.constant = -self.view.bounds.width
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        downloadInformLabel.textColor = .black
        DispatchQueue.main.async {
            self.animateListView()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.didEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
            NotificationCenter.default.addObserver(self , selector: #selector(self.donePlaying) , name: NSNotification.Name.AVPlayerItemDidPlayToEndTime , object: nil)
            self.playerCancelButton.addTarget(self, action: #selector(self.playerCancelButtonAction), for: .touchUpInside)
            self.retryButton.addTarget(self, action: #selector(self.retryButtonAction), for: .touchUpInside)
        }
    }
    
    @IBAction func changecolor(_ sender: UIButton) {
        switch sender {
        case yellow:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 24/255, alpha: 1.0)
                self.wkWebView.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 24/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 24
                colorcode.green = 212
                colorcode.imagesto = "yellow"
                self.refreshArticle()
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case blue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1.0)
                self.wkWebView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1.0)
                colorcode.red = 0
                colorcode.blue = 255
                colorcode.green = 0
                colorcode.imagesto = "blue"
                self.refreshArticle()
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
        
        case pink:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
                self.pink.backgroundColor = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
                self.wkWebView.backgroundColor = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 245
                colorcode.green = 177
                colorcode.imagesto = "pink"
                self.refreshArticle()
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case green:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0)
                self.wkWebView.backgroundColor = UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0)
                colorcode.red = 156
                colorcode.blue = 72
                colorcode.green = 222
                colorcode.imagesto = "lightgreen"
                self.refreshArticle()
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case red:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
                self.wkWebView.backgroundColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)

                colorcode.red = 225
                colorcode.blue = 0
                colorcode.green = 0
                colorcode.imagesto = "red"
                self.refreshArticle()
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case lightblue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)
                self.wkWebView.backgroundColor = UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)
                
                colorcode.red = 150
                colorcode.blue = 255
                colorcode.green = 239
                colorcode.imagesto = "lightblue"
                self.refreshArticle()
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        default:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 240/255, blue: 255/255, alpha: 1.0)
                self.wkWebView.backgroundColor = UIColor(red: 150/255, green: 240/255, blue: 255/255, alpha: 1.0)
                
                colorcode.imagesto = "lightblue"
                self.refreshArticle()
                self.wkWebView.isOpaque = false
            }
            return
        }
    }
    
    @objc func tap(_ sender: AnyObject) {
        self.navigationController?.popViewControllerWithEaseOut(animated: false)
    }
    
    private func setupView() {
        rightHeaderButtonLeadingConstraint.constant =  self.view.bounds.width
        
        leadingConstraintConstant = 0.0
        topConstraintConstant = 0.0
        var heightConstraintConstant: CGFloat = 30.0
        let breadcrumbViewWidth = self.view.frame.width - 80
        
        for view in self.breadcrumbView.subviews {
            if view != rightHeaderButton {
                view.removeFromSuperview()
            }
        }
        
        var tag = 0
        for title in breadcrumbTitlesArray {
            let button = UIButton()
            button.tag = tag
            tag = tag + 1
            button.titleLabel?.font = rightHeaderButton.titleLabel?.font
            button.contentHorizontalAlignment = .leading
            button.addTarget(self, action: #selector(self.breadcrumbButtonAction(_:)), for: .touchUpInside)
            
            button.translatesAutoresizingMaskIntoConstraints = false
            
            if title.count > 40 {
                let label = UILabel()
                label.textColor = .black
                label.font = UIFont.init(name: "BureauGroteskFBOneFive", size: 22)
                label.frame.size.width = 350.0
                label.numberOfLines = 2
                label.text = " > \(title)"
                
                button.titleLabel?.numberOfLines = 2
                button.titleLabel?.lineBreakMode = .byWordWrapping
                
                button.setTitle(label.text, for: .normal)
                self.breadcrumbViewHeightConstraint.constant = self.breadcrumbViewHeightConstraint.constant + 30
            } else {
                button.setTitle(" > \(title)", for: .normal)
            }
            
            button.setTitleColor(UIColor.white, for: .normal)
            button.sizeToFit()
            
            breadcrumbView.addSubview(button)
            self.breadcrumbBtn.append(button)
            
            if leadingConstraintConstant + button.frame.width > breadcrumbViewWidth {
                leadingConstraintConstant = 0
                topConstraintConstant = topConstraintConstant + button.frame.height
                heightConstraintConstant = topConstraintConstant + button.frame.height
                breadcrumbView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 10).isActive = true
                button.trailingAnchor.constraint(equalTo: breadcrumbView.trailingAnchor, constant: 10).isActive = true
            }
            button.topAnchor.constraint(equalTo: breadcrumbView.topAnchor, constant: topConstraintConstant).isActive = true
            button.leadingAnchor.constraint(equalTo: breadcrumbView.leadingAnchor, constant: leadingConstraintConstant).isActive = true
            
            leadingConstraintConstant = leadingConstraintConstant + button.frame.width
            breadcrumbViewHeightConstraint.constant = heightConstraintConstant
        }
        rightHeaderButton.tag = tag
    }
    
    @objc func breadcrumbButtonAction(_ sender: UIButton) {
        if sender == rightHeaderButton {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[sender.tag + 3], animated: false)
            }
        } else {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[sender.tag + 3], animated: false)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //MBProgressHUD.hide(for: view, animated: true)
        if progressView.progress != 0 {
            download!.downloadTask?.cancel()
        }
            
        if playerView.isHidden == false {
            player.pause()
            playerView.isHidden = true
            playButton.setImage(UIImage(named: "Play.png"), for: UIControlState.normal)
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func willEnterForeground() {
        if progressView.progress != 0 {
            downloadProgressUpdate(for: 0.0)
            self.handleDownloadInfoView()
        }
    }
        
    @objc func didEnterBackground() {
        if progressView.progress != 0 {
            download!.downloadTask?.cancel()
        }
        playButton!.setImage(UIImage(named: "Play.png"), for: UIControlState.normal)
    }
    
    @objc func retryButtonAction(_ sender: UIButton) {
        if Connectivity.isConnectedToInternet {
            UIView.animate(withDuration: 2) {
                self.handleDownloadInfoView()
                if (self.request.url?.scheme == "audio") || (self.request.url?.scheme == "video") {
                    self.showMediaController(request: self.request)
                }
            }
        } else {
            UIView.animate(withDuration: 2) {
                self.blurView.alpha = 0.8
                self.alertView2.isHidden = false
            }
        }
    }
    
    func animateListView() {
        webviewLeadinContraint.constant = 0
        trailingLeadingContraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func handleDownloadInfoView() {
        UIView.animate(withDuration: 3.0) {
            if self.downloadInformView.alpha == 0 {
                self.downloadInformView.alpha = 1
            } else {
                self.downloadInformView.alpha = 0
            }
        }
    }
    
    @objc func playerCancelButtonAction(_ sender: UIButton) {
        //MBProgressHUD.hide(for: view, animated: true)
        if progressView.progress != 0 {
            download!.downloadTask?.cancel()
        }
            
        if playerView.isHidden == false {
            player.pause()
            playerView.isHidden = true
            playButton.setImage(UIImage(named: "Play.png"), for: UIControlState.normal)
        }
        NotificationCenter.default.removeObserver(self)
        navigationController?.popViewControllerWithEaseOut(animated: true)
    }
    
    func refreshArticle() {
        let htmlFilePath = Bundle.main.path(forResource: htmlName, ofType: "html")!
        if colorcode.imagesto == "blue" {
            for btn in self.breadcrumbBtn {
                UIView.animate(withDuration: 2.0) {
                    btn.setTitleColor(.white, for: .normal)
                }
            }
            do {
                let htmlString = try String(contentsOfFile: htmlFilePath)
                let cssString = "<link href=\"style2.css\" type=\"text/css\" rel=\"stylesheet\"/>" + "<link href=\"VideoPlayer.css\" type=\"text/css\" rel=\"stylesheet\"/>" + "<link href=\"VideoPlayer.min.js\" type=\"text/css\" rel=\"stylesheet\"/>" + "<link href=\"playbtn.css\" type=\"text/css\" rel=\"stylesheet\"/>" + htmlString
                self.wkWebView.loadHTMLString(cssString, baseURL: Bundle.main.bundleURL)
            } catch {
                print("Problem with loading a html file! Please stick around.")
            }
        } else {
            for btn in self.breadcrumbBtn {
                UIView.animate(withDuration: 2.0) {
                    btn.setTitleColor(.black, for: .normal)
                }
            }
            do {
                let htmlString = try String(contentsOfFile: htmlFilePath)
                let cssString = "<link href=\"style.css\" type=\"text/css\" rel=\"stylesheet\"/>" + "<link href=\"VideoPlayer.css\" type=\"text/css\" rel=\"stylesheet\"/>" + "<link href=\"VideoPlayer.min.js\" type=\"text/css\" rel=\"stylesheet\"/>" + "<link href=\"playbtn.css\" type=\"text/css\" rel=\"stylesheet\"/>" + htmlString
                self.wkWebView.loadHTMLString(cssString, baseURL: Bundle.main.bundleURL)
            } catch {
                print("Problem with loading a html file! Please stick around.")
            }
        }
    }
    
    // if logo touched then it goes to  home menu
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        if (gesture.view as? UIImageView) != nil {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[2], animated: false)
            }
        }
    }
    
    func removePeriodicTimeObserver() {
        if let token = timeObserverToken {
            player.removeTimeObserver(token)
            timeObserverToken = nil
        }
    }
    
    @objc func donePlaying() {
        UIView.animate(withDuration: 2.0) {
            self.playerView.isHidden = true
            if self.informLabelTimer != nil {
                self.informLabelTimer.invalidate()
                self.informLabelTimer = nil
            }
        }
    }

    @IBAction func backButtonAction(_ sender: UIButton) {
        //MBProgressHUD.hide(for: view, animated: true)
        if progressView.progress != 0 {
            download!.downloadTask?.cancel()
        }
            
        if playerView.isHidden == false {
            player.pause()
            playerView.isHidden = true
            playButton.setImage(UIImage(named: "Play.png"), for: UIControlState.normal)
        }
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.popViewController(animated: false)
    }
    
    func callWhileSpeedChange(networkStatus: NetworkStatus) {
        switch networkStatus {
            case .poor:
                DispatchQueue.main.async {
                    self.downloadInformLabel.text = "Hmmmm...this is taking longer than usual."
                }
                break
            case .good:
                DispatchQueue.main.async {
                    self.downloadInformLabel.text = "Your file is downloading and buffering..."
                }
                break
            case .disConnected:
                break
        }
    }
}

extension MCWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        
        let request = navigationAction.request
        if request.url?.host == "itunes.apple.com" {
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            decisionHandler(.allow)
        } else {
            if (request.url?.scheme == "audio") || (request.url?.scheme == "video") {
                self.request = request
                DispatchQueue.main.async {
                    if request.url?.scheme == "video" {
                        self.playerView.isHidden = false
                    }
                }
                showMediaController(request: request)
            } else if (request.url?.scheme == "http" || request.url?.scheme == "https") {
                showurls(request: request)
            } else {
                decisionHandler(.allow)
                return
            }
            decisionHandler(.cancel)
        }
    }
}

extension MCWebViewController {
    fileprivate func showMediaController(request: URLRequest) {
        if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCMediaViewController") as? MCMediaViewController {
            controller.request = request
            let name = request.url?.absoluteString
        
            let array = name!.components(separatedBy: ":")
            print(request.url!)

            controller.mediaUrlString = array[1] + ":" + array[2]
            self.urlString = array[1] + ":" + array[2]
            
            controller.breadcrumbTitlesArray = self.breadcrumbTitlesArray
            
            if request.url?.scheme == "audio" {
                if LocalStorageManager.isLocalFileExistForUrl(controller.mediaUrlString) {
                    controller.playOfflineMedia(fromController: self)
                } else {
                    controller.fromViewController = self
                    self.navigationController?.pushViewControllerWithEaseIn(controller, animated: false)
                }
            }
            if request.url?.scheme == "video" {
                playerView.isHidden = false
                if LocalStorageManager.isLocalFileExistForUrl(urlString) {
                    playOfflineMedia()
                } else {
                    if Connectivity.isConnectedToInternet {
                        if self.informLabelTimer == nil {
                            self.informLabelTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.handleDownloadInfoView), userInfo: nil, repeats: true)
                        } else {
                            self.informLabelTimer.invalidate()
                            self.informLabelTimer = nil
                        }
                        self.handleDownloadInfoView()
                        self.blurView.alpha = 0
                        if URL(string: self.urlString) != nil {
                            let name = request.url?.absoluteString
                            let array = name!.components(separatedBy: ":")
                            playVideo(NSURL(string: array[1] + ":" + array[2])! as URL)
                        }
                        if progressView.progress == 0 {
                            self.downloadFile()
                        }
                    } else {
                        UIView.animate(withDuration: 2.0) {
                            self.blurView.alpha = 0.8
                            self.alertView2.isHidden = false
                        }
                    }
                }
            }
            if ListViewController.breadcrumptittle.contains(urltouse) {
                
            } else {
                ListViewController.breadcrumptittle.append(htmlName)
                ListViewController.breadcrumptittle.append(urltouse)
            }
        }
    }
    
    fileprivate func showurls(request: URLRequest) {
        if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "openUrlViewController") as? openUrlViewController {
            
            let name = request.url?.absoluteString
            let array = name!.components(separatedBy: ":")
            
            let urlsforwebview = array[0] + ":" + array[1]
            
            controller.mediaUrlString = urlsforwebview
            self.navigationController?.pushViewControllerWithEaseIn(controller, animated: false)
        }
    }
}

extension MCWebViewController {
    func playOfflineMedia() {
        if let mediaFileUrl = LocalStorageManager.localFilePathForUrl(urlString) {
            playVideo(mediaFileUrl)
        }
    }

    func playVideo(_ mediaUrl: URL) {
        if player != nil {
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                try AVAudioSession.sharedInstance().setActive(true)
            } catch(let error) {
                print(error.localizedDescription)
            }
            player.pause()
            player.seek(to: kCMTimeZero)
            addTimeObserver()
            player.playImmediately(atRate: 1.0)
        } else {
            let urlAsset = AVAsset(url: mediaUrl)
            let playerItem = AVPlayerItem(asset: urlAsset)
            player = AVPlayer(playerItem: playerItem)
            
            setupAVPlayer()
    
            let duration: CMTime = playerItem.asset.duration
            let seconds: Float64 = CMTimeGetSeconds(duration)
            
            playerLayer = AVPlayerLayer(player: player)
            self.playerView.layer.addSublayer(playerLayer)
            
            playerLayer.frame = self.playerView.bounds
            playerLayer.videoGravity = .resizeAspect
            
            self.playerView.bringSubview(toFront: cornerView)
            self.playerView.bringSubview(toFront: overlayView)
            
            playbackSlider.minimumValue = 0
            
            playbackSlider.setThumbImage(UIImage(named: "ThumbCircle.png"), for: .normal)
            playButton!.tintColor = UIColor.white
            
            addTimeObserver()
            
            playbackSlider.maximumValue = Float(seconds)
            playbackSlider.isContinuous = true
            playbackSlider.tintColor = UIColor.lightGray
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                try AVAudioSession.sharedInstance().setActive(true)
            } catch(let error) {
                print(error.localizedDescription)
            }
            player.seek(to: kCMTimeZero)
        }
    }

    private func setupAVPlayer() {
        player.addObserver(self, forKeyPath: "status", options: [.old, .new], context: nil)
            if #available(iOS 10.0, *) {
                self.observer = player.observe(\.rate, options: [.new, .old], changeHandler: { (player, change) in
                    if player.rate == 1  {
                        print("Playing")
                    } else {
                        print("Stop")
                    }
                })
            player.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
        } else {
            player.addObserver(self, forKeyPath: "rate", options: [.old, .new], context: nil)
        }
    }
        
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object as AnyObject? === player {
            //MBProgressHUD.hide(for: view, animated: true)
            if keyPath == "status" {
                if player.status == .readyToPlay {
                    //MBProgressHUD.showAdded(to: self.playerView, animated: true)
                    self.playerView.bringSubview(toFront: self.cornerView)
                    player.playImmediately(atRate: 1.0)
                }
            } else if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
                if #available(iOS 10.0, *) {
                    let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
                    let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
                    if newStatus != oldStatus {
                        DispatchQueue.main.async {[weak self] in
                            if newStatus == .playing || newStatus == .paused {
                                self!.setPlayerObservers()
                                //MBProgressHUD.hide(for: (self?.playerView)!, animated: true)
                                UIView.animate(withDuration: 4.0) {
                                    self!.downloadInformView.alpha = 0
                                }
                            } else {
                    
                            }
                        }
                    }
                }
            } else if keyPath == "rate" {
                if player.rate > 0 {
        
                }
            }
        }
    }
        
    func addTimeObserver() {
        let interval = CMTime(seconds: 1, preferredTimescale: 2)
        timeObserverToken = player.addPeriodicTimeObserver(forInterval: interval, queue: .main) { [weak self] time in
            guard let currentItem = self?.player.currentItem else {return}
            self?.playbackSlider.maximumValue = Float(currentItem.duration.seconds)
            self?.playbackSlider.minimumValue = 0
            self?.playbackSlider.value = Float(currentItem.currentTime().seconds)
            if self?.player.rate != 0 {
                self?.currentTimeLabel.text = self?.getTimeString(from: currentItem.currentTime())
            }
        } as AnyObject
    }
        
    func setPlayerObservers() {
        if self.player.rate != 0 {
            self.durationLabel.text = getTimeString(from: player.currentItem!.duration)
        }
        playbackSlider.addTarget(self, action: #selector(MCWebViewController.playbackSliderValueChanged(_:)), for: .valueChanged)
        playButton!.addTarget(self, action: #selector(MCWebViewController.playButtonTapped(_:)), for: .touchUpInside)
        forwardButton.addTarget(self, action: #selector(MCWebViewController.forwardPressed(_:)), for: .touchUpInside)
        backwardButton.addTarget(self, action: #selector(MCWebViewController.backwardPressed(_:)), for: .touchUpInside)
    }
        
    func getTimeString(from time: CMTime) -> String {
        let totalSeconds = CMTimeGetSeconds(time)
        let hours = Int(totalSeconds/3600)
        let minutes = Int(totalSeconds/60) % 60
        let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        if hours > 0 {
            return String(format: "%i:%02i:%02i", arguments: [hours, minutes, seconds])
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
    }
    
    @objc func playbackSliderValueChanged(_ playbackSlider: UISlider) {
        let newTime = CMTime(seconds: Double(playbackSlider.value), preferredTimescale: 600)
        player.seek(to: newTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
            
    @objc func playButtonTapped(_ sender: UIButton) {
        if player?.rate == 0 {
            player!.play()
            playButton.setImage(UIImage(named: "Pause.png"), for: UIControlState.normal)
        } else {
            player!.pause()
            playButton.setImage(UIImage(named: "Play.png"), for: UIControlState.normal)
        }
    }
        
    @objc func forwardPressed(_ sender: UIButton) {
        guard let duration = player.currentItem?.duration else {return}
        let currentTime = CMTimeGetSeconds(player.currentTime())
        let newTime = currentTime + 10.0
        if newTime < (CMTimeGetSeconds(duration) - 10.0) {
            let time: CMTime = CMTimeMake(Int64(newTime*1000), 1000)
            player.seek(to: time)
        }
    }
        
    @objc func backwardPressed(_ sender: UIButton) {
        let currentTime = CMTimeGetSeconds(player.currentTime())
        var newTime = currentTime - 10.0
        if newTime < 0 {
            newTime = 0
        }
        let time: CMTime = CMTimeMake(Int64(newTime*1000), 1000)
        player.seek(to: time)
    }

    func downloadFile() {
        DownloadManager.shared.setup()
        self.download = Download(url: self.urlString)
        self.download.delegate = self
        DownloadManager.shared.downloadMedia(for: self.download)
        test.networkSpeedTestStart(UrlForTestSpeed: self.urlString)
    }
}
    
extension MCWebViewController: DownloadDelegate {
    func downloadProgressUpdated(for progress: Float) {
        print("progress is......\(progress)")
        let progessInt = Int(progress * 100)
        DispatchQueue.main.async {
            self.progressView.progress = progress
            self.downloadProgressLabel.text =  String(progessInt) + "%"
            if progessInt == 100 {
                UIView.animate(withDuration: 4.0) {
                    self.downloadInformView.alpha = 0
                    if self.informLabelTimer != nil {
                        self.informLabelTimer.invalidate()
                        self.informLabelTimer = nil
                    }
                }
                //Play media
                UIView.animate(withDuration: 2.0) {
                    self.blurView.alpha = 0
                    self.alertView2.isHidden = true
                }
            }
        }
    }

    func downloadProgressUpdate(for progressValue: Float) {
         print("progress is......\(progressValue)")
         let progessInt = Int(progressValue * 100)
         DispatchQueue.main.async {
             self.progressView.progress = progressValue
             self.downloadProgressLabel.text = String(progessInt) + "%"
             if progessInt == 100 {
                UIView.animate(withDuration: 4.0) {
                    self.downloadInformView.alpha = 0
                    if self.informLabelTimer != nil {
                        self.informLabelTimer.invalidate()
                        self.informLabelTimer = nil
                    }
                }
             } else {
                do {
                   sleep(2)
                }
            }
        }
    }
}

protocol NetworkSpeedProviderDelegate: class {
    func callWhileSpeedChange(networkStatus: NetworkStatus)
        
    }

public enum NetworkStatus: String {
    case poor; case good; case disConnected
}

class NetworkSpeedTest: UIViewController {
    weak var delegate: NetworkSpeedProviderDelegate?
    var startTime = CFAbsoluteTime()
    var stopTime = CFAbsoluteTime()
    var bytesReceived: CGFloat = 0
    var testURL: String?
    var speedTestCompletionHandler: ((_ megabytesPerSecond: CGFloat, _ error: Error?) -> Void)? = nil
    var timerForSpeedTest: Timer?
    
    func networkSpeedTestStart(UrlForTestSpeed:String!){
        testURL = UrlForTestSpeed
        timerForSpeedTest = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(testForSpeed), userInfo: nil, repeats: true)
    }
    
    func networkSpeedTestStop() {
        timerForSpeedTest?.invalidate()
    }
    
    @objc func testForSpeed() {
        testDownloadSpeed(withTimout: 2.0, completionHandler: {(_ megabytesPerSecond: CGFloat, _ error: Error?) -> Void in
            print("%0.1f; KbPerSec = \(megabytesPerSecond)")
            if (error as NSError?)?.code == -1009 {
                self.delegate?.callWhileSpeedChange(networkStatus: .disConnected)
            } else if megabytesPerSecond == -1.0 {
                self.delegate?.callWhileSpeedChange(networkStatus: .poor)
            } else {
                self.delegate?.callWhileSpeedChange(networkStatus: .good)
            }
        })
    }
}

extension NetworkSpeedTest: URLSessionDataDelegate, URLSessionDelegate {
    func testDownloadSpeed(withTimout timeout: TimeInterval, completionHandler: @escaping (_ megabytesPerSecond: CGFloat, _ error: Error?) -> Void) {

        // you set any relevant string with any file
        let urlForSpeedTest = URL(string: testURL!)

        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0
        speedTestCompletionHandler = completionHandler
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)

        guard let checkedUrl = urlForSpeedTest else { return }

        session.dataTask(with: checkedUrl).resume()
    }

    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived += CGFloat(data.count)
        stopTime = CFAbsoluteTimeGetCurrent()
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        let elapsed = (stopTime - startTime) //as? CFAbsoluteTime
        let speed: CGFloat = elapsed != 0 ? bytesReceived / (CGFloat(CFAbsoluteTimeGetCurrent() - startTime)) / 1024.0 : -1.0
        // treat timeout as no error (as we're testing speed, not worried about whether we got entire resource or not
        if error == nil || ((((error as NSError?)?.domain) == NSURLErrorDomain) && (error as NSError?)?.code == NSURLErrorTimedOut) {
            speedTestCompletionHandler?(speed, nil)
        } else {
            speedTestCompletionHandler?(speed, error)
        }
    }
}
