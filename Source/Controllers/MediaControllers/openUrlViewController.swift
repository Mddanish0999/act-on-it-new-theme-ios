//
//  openUrlViewController.swift
//  Mindfulness
//
//  Created by localadmin on 5/14/19.
//  Copyright © 2019 Mindful Creations. All rights reserved.
//

import UIKit
import WebKit

class openUrlViewController: UIViewController, WKUIDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var logoImage: UIImageView!
    
    var mediaUrlString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ListViewController.imageTapped(gesture:)))
        logoImage.addGestureRecognizer(tapGesture)
        logoImage.isUserInteractionEnabled = true
        
        let webUrl: NSURL = NSURL(string: mediaUrlString)!
    
        print(mediaUrlString)
  
        let webRequest: NSURLRequest = NSURLRequest(url: webUrl as URL)
        webView.load(webRequest as URLRequest)
    }
    
    @IBAction func moreMenuButton(_ sender: UIButton) {
        if let MoreViewController = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "MoreViewController") as? MoreViewController {
            self.navigationController?.pushViewController(MoreViewController, animated: true)
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

    @objc func imageTapped(gesture: UIGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if (gesture.view as? UIImageView) != nil {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[2], animated: false)
            }
        }
    }
}
