import UIKit

typealias JSON = [String: Any]

final class DownloadManager: NSObject {
    
    static var shared = DownloadManager()
    var activeDownloads: [String: Download]?
    weak var defaultSession: URLSession?// = URLSession(configuration: .default)
    
    // MARK: - Main session used
    
    weak var downloadsSession : URLSession? {
        get {
            if let _ = defaultSession {
                
                return defaultSession
            } else {
                
                let config = URLSessionConfiguration.background(withIdentifier: "mindfulnessbackground")
                weak var queue = OperationQueue()
                
                defaultSession = URLSession(configuration: config, delegate: self, delegateQueue: queue)
                return defaultSession
            }
        }
    }
    
    func setup() {
        activeDownloads = [String: Download]()
    }
    
    
    // MARK: - Transitory session
    
    internal static func downloadData(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession(configuration: .ephemeral).dataTask(with: URLRequest(url: url)) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    // MARK: - Turns data into JSON - JSON is typealias
    
    fileprivate static func convertToJSON(with data: Data) -> JSON? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? JSON
        } catch {
            return nil
        }
    }
}

// MARK: - URLSessionDownloadDelegate

extension DownloadManager: URLSessionDownloadDelegate {
    
    func downloadMedia(for download: Download?) {
        
        if let download = download,
            let urlString = download.url,
            let url = URL(string: urlString) {
            activeDownloads?[urlString] = download
            download.downloadTask = downloadsSession?.downloadTask(with: url)
            download.downloadTask?.resume()
        }
    }
    
}

// MARK: - URLSessionDelegate

extension DownloadManager: URLSessionDelegate {
    
    internal func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let completionHandler = appDelegate.backgroundSessionCompletionHandler {
            appDelegate.backgroundSessionCompletionHandler = nil
            DispatchQueue.main.async {
                completionHandler()
            }
        }
    }
    
    internal func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64,totalBytesExpectedToWrite: Int64) {
        if let downloadUrl = downloadTask.originalRequest?.url?.absoluteString,
            let download = activeDownloads?[downloadUrl] {
            download.progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
        }
    }
}

extension DownloadManager {
    
    internal func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        if let originalURL = downloadTask.originalRequest?.url?.absoluteString {
            let destinationURL = LocalStorageManager.localFilePathForUrl(originalURL)
            let fileManager = FileManager.default
            
            do {
                if let destinationURL = destinationURL {
                    
                    _  = try fileManager.replaceItemAt(destinationURL, withItemAt: location)
                    //try fileManager.copyItem(at: location, to: destinationURL)
                    
                    print("copy file to PATH: \(destinationURL.description)")
                    
                }
            } catch let error {
                print("Could not copy file to disk: \(error.localizedDescription)")
            }
        }
    }
    
    func URLSessionDidFinishEventsForBackgroundURLSession(session: URLSession) {
        print("Session: \(session)")
    }
}




