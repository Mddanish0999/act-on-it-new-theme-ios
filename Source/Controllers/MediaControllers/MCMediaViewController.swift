//
//  MCMediaViewController.swift
//  Mindfulness
//
//  Created by Rajinder on 9/28/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import AVKit
import Pastel
import Foundation
import AVFoundation

class MCMediaViewController: UIViewController, AVPlayerViewControllerDelegate, NetworkSpeedProviderDelegate {

    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var DownloadButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView! {
        didSet {
            progressView.progress = 0
        }
    }

    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var alertView2: UIView!
    @IBOutlet weak var cancelImage: UIImageView!
    @IBOutlet weak var retryButton: UIButton!

    var fromViewController: UIViewController?
    var mediaUrlString: String!
    var request: URLRequest!
    var download: Download!
    
    var breadcrumbTitlesArray = [String]()
    
    let test = NetworkSpeedTest()

    var downloads: [String: Download]? = nil
    var colorCodes = colorcode()
    
    static var red = 0.0
    static var blue = 0.0
    static var green = 0.0
    static var backcolor = " "

    @IBOutlet weak var downloadProgressLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        playButton.isHidden = true
        DownloadButton.isHidden = true
        tittle.isHidden = true
        
        test.delegate = self
        test.networkSpeedTestStop()
        
        cancelImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        retryButton.addTarget(self, action: #selector(retryButtonAction), for: .touchUpInside)
        setupView()
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
        
        }
    }
    
    @objc func tap(_ sender: AnyObject) {
         self.navigationController?.popViewController(animated: false)
    }
    
    @objc func willEnterForeground() {
        if alertView.isHidden == false {
            downloadProgressUpdate(for: 0.0)
        } else {
            print("app enter to foreground at media")
        }
     }
    
    @objc func didEnterBackground() {
        if alertView.isHidden == false {
             download!.downloadTask?.cancel()
        } else {
            print("App enter to background at media")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if LocalStorageManager.isLocalFileExistForUrl(mediaUrlString) {
            playOfflineMedia(fromController: self)
        } else {
            if Connectivity.isConnectedToInternet {
                alertView.isHidden = false
                blurView.isHidden = true
                downloadFile()
                if URL(string: mediaUrlString) != nil {
                    let name = request!.url?.absoluteString
                    let array = name!.components(separatedBy: ":")
                    playAudio(NSURL(string: array[1] + ":" + array[2])! as URL)
               }
            } else {
                alertView.isHidden = true
                blurView.isHidden = false
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callWhileSpeedChange(networkStatus: NetworkStatus) {
        switch networkStatus {
            case .poor:
                DispatchQueue.main.async {
                    if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCAudioPlayerController") as? MCAudioPlayerController {
                        controller.downloadInformLabel.text = "Hmmmm...this is taking longer than usual."
                    }
                }
                break
            case .good:
                DispatchQueue.main.async {
                    if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCAudioPlayerController") as? MCAudioPlayerController {
                        controller.downloadInformLabel.text = "Your file is downloading and buffering..."
                    }
                }
                break
            case .disConnected:
                break
        }
    }

    private func setupView() {
        self.progressView.progress = 0
        self.downloadProgressLabel.text = "0%"
        DispatchQueue.main.async {
            self.setGradientBackground(view: self.centerView)
        }
    }

    @IBAction func closeButtonAction(_ sender: Any) {
        self.navigationController?.popViewControllerWithEaseOut(animated: false)
    }
    
    func downloadFile() {
        DownloadManager.shared.setup()
        
        download = Download(url: mediaUrlString)
        download.delegate = self
        DownloadManager.shared.downloadMedia(for: download)
        test.networkSpeedTestStart(UrlForTestSpeed: mediaUrlString)
    }
    
    @objc func retryButtonAction(_ sender: UIButton) {
        if Connectivity.isConnectedToInternet {
            UIView.animate(withDuration: 2.0) {
                self.blurView.alpha = 0
                self.alertView.isHidden = false
                self.downloadFile()
            }
           if URL(string: self.mediaUrlString) != nil {
                let name = self.request!.url?.absoluteString
                let array = name!.components(separatedBy: ":")
                self.playAudio(NSURL(string: array[1] + ":" + array[2])! as URL)
            }
        } else {
            self.alertView.isHidden = true
            self.blurView.isHidden = false
        }
    }

    @IBAction func downloadButtonAction(_ sender: Any) {
        DownloadManager.shared.setup()
        
        download = Download(url: mediaUrlString)
        download.delegate = self
        DownloadManager.shared.downloadMedia(for: download)
    }

    @IBAction func playButtonAction(_ sender: Any) {
        if let mediaUrl = URL(string: mediaUrlString) {
            if request.url?.scheme == "audio" {
                let name = request!.url?.absoluteString
                let array = name!.components(separatedBy: ":")
                print(request.url!)
                print(mediaUrl)
                playAudio(NSURL(string: array[1] + ":" + array[2])! as URL)
                
                ListViewController.breadcrumptittle.append(array[2])
            } else if request.url?.scheme == "video" {
                let name = request!.url?.absoluteString
                let array = name!.components(separatedBy: ":")
                print(request.url!)
                print(mediaUrl)
               
                playVideo(NSURL(string: array[1] + ":" + array[2])! as URL)
                print(NSURL(string: array[1] + ":" + array[2])! as URL)
            }
        }
    }
}
//MARK:- DownloadDelegate
extension MCMediaViewController: DownloadDelegate {
    func downloadProgressUpdated(for progress: Float) {
        print("progress is......\(progress)")
        let progessInt =  Int(progress * 100)
        DispatchQueue.main.async {
            self.progressView.progress = progress
            self.downloadProgressLabel.text =  String(progessInt) + "%"
            if progessInt == 100 {
                if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCAudioPlayerController") as? MCAudioPlayerController {
                    
                    controller.downloadInformLabel.text = "Your file has downloaded..."
                    controller.downloadInformView.alpha = 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        UIView.animate(withDuration: 4.0) {
                            controller.downloadInformView.alpha = 0
                        }
                    }
                }
            }
        }
    }
    
    func downloadProgressUpdate(for progressValue: Float) {
         print("progress is......\(progressValue)")
         let progessInt =  Int(progressValue * 100)
         DispatchQueue.main.async {
            self.progressView.progress = progressValue
            self.downloadProgressLabel.text =  String(progessInt) + "%"
            if progessInt == 100 {
                if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCAudioPlayerController") as? MCAudioPlayerController {
                    
                    controller.downloadInformLabel.text = "Your file has downloaded..."
                    controller.downloadInformView.alpha = 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        UIView.animate(withDuration: 4.0) {
                            controller.downloadInformView.alpha = 0
                        }
                    }
                }
            } else {
                do {
                    sleep(2)
                }
                self.downloadFile()
            }
        }
    }
}
//MARK:- Play Audio, Video
extension MCMediaViewController {
    func playOfflineMedia(fromController: UIViewController) {
        self.fromViewController = fromController
        playOfflineMedia()
    }

    func playOfflineMedia() {
        if let mediaFileUrl = LocalStorageManager.localFilePathForUrl(mediaUrlString) {
            if request.url?.scheme == "audio" {
                _ = request!.url?.absoluteString
                print(mediaFileUrl)
                 playAudio(mediaFileUrl)
            } else if request.url?.scheme == "video" {
                playVideo(mediaFileUrl)
            }
        }
    }

    func playAudio(_ mediaUrl: URL) {
        if let controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCAudioPlayerController") as? MCAudioPlayerController {
            controller.audioURL = mediaUrl
            controller.breadcrumbTitlesArray = self.breadcrumbTitlesArray
            controller.mediaUrlString = self.mediaUrlString
            if let fromController = self.fromViewController {
                self.popMediaController()
                fromController.navigationController?.pushViewControllerWithEaseIn(controller, animated: false)
            } else {
                self.navigationController?.pushViewControllerWithEaseIn(controller, animated: false)
            }
        }
    }

    func playVideo(_ mediaUrl: URL) {
        let player = AVPlayer(url: mediaUrl)
        let vc = AVPlayerViewController()
        vc.player = player
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            let player = AVPlayer(url: mediaUrl)
            let vc = AVPlayerViewController()
            vc.player = player
        }
        catch {
            print("============================")
        }
        if let controller = self.fromViewController {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // in half a second...
                controller.present(vc, animated: true) {
                    vc.player?.play()
                }
            }
              self.popMediaController()
        }
        else {
            self.present(vc, animated: true) {
                vc.player?.play()
                 self.popMediaController()
            }
        }
        //   NotificationCenter.default.addObserver(self, selector: #selector(MCHtmlViewerCell.playbackComplete), name: NSNotification.Name.MPMoviePlayerPlaybackDidFinish, object: nil)
    }
    
    fileprivate func popMediaController() {
           if let navigationController = self.navigationController {
            navigationController.popViewController(animated: false)
        }
    }
}
