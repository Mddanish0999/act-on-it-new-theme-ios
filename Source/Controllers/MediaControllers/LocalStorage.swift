import UIKit

final class LocalStorageManager {
    
    static func localFilePathForUrl(_ url: String) -> URL? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let mediaDirectory = documentsPath.appendingPathComponent("Media") as NSString
        if let url = URL(string: url) {
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: mediaDirectory as String) {
                do {
                    try fileManager.createDirectory(atPath: mediaDirectory as String, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    NSLog("Couldn't create Media directory")
                }
            }
            
            let fullPath = mediaDirectory.appendingPathComponent(url.lastPathComponent)
            return URL(fileURLWithPath: fullPath)
        }
        return nil
    }
    
    static func isLocalFileExistForUrl(_ url: String) -> Bool {
        var isExist = false
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let mediaDirectory = documentsPath.appendingPathComponent("Media") as NSString
        if let url = URL(string: url) {
            
            let filePath = mediaDirectory.appendingPathComponent(url.lastPathComponent)
            if FileManager.default.fileExists(atPath: filePath) {
                isExist = true
            }
        }
        return isExist
    }
    
    static func getDownloadedFiles() -> [[String: Any]]? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let mediaDirectory = documentsPath.appendingPathComponent("Media") as NSString
        
        let fileManager = FileManager.default
        
        var fileList = [[String: Any]]()
        
        do {
            let list = try fileManager.contentsOfDirectory(atPath: mediaDirectory as String)
            for file in list {
                let filePath = mediaDirectory.appendingPathComponent(file)
                
                do {
                    let attributes = try fileManager.attributesOfItem(atPath: filePath)
                    if let fileSize =  attributes[FileAttributeKey.size] as? UInt {
                        let sizeInMB = ((Double(fileSize))/1024)/1024
                        fileList.append(["name": file, "size": sizeInMB])
                    }
                } catch {
                    print("Error getting file attributes \(mediaDirectory): \(error.localizedDescription)")
                }
            }
        } catch {
            print("Error while enumerating files \(mediaDirectory): \(error.localizedDescription)")
        }
        return fileList
    }
    
    
    static func removeDownloadedFiles(files: [String]) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let mediaDirectory = documentsPath.appendingPathComponent("Media") as NSString
        let fileManager = FileManager.default
        
        for file in files {
            let filePath = mediaDirectory.appendingPathComponent(file)
            try? fileManager.removeItem(atPath: filePath)
        }
    }
}
