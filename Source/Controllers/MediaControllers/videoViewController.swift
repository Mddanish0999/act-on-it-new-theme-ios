//
//  videoViewController.swift
//  Mindfulness
//
//  Created by localadmin on 9/4/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import WebKit
import Photos
class videoViewController: UIViewController,WKUIDelegate, WKNavigationDelegate {
    var htmlName: String!
    @IBOutlet weak var wkview: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        wkview.uiDelegate = self
        wkview.navigationDelegate = self
        
        if let htmlPath = Bundle.main.path(forResource: htmlName, ofType: "m4v")
        {
            wkview.load(URLRequest(url: URL(fileURLWithPath: htmlPath)))
        }
        
//        let transition = CATransition()
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        wkview.layer.add(transition, forKey: nil)
//        wkview.addSubview(wkview)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

