//
//  MCBaseViewController.swift
//  Mindfulness
//
//  Created by Dinesh Raja on 10/3/15.
//  Copyright © 2015 Mindful Creations. All rights reserved.
//

import UIKit

class MCBaseViewController: UIViewController {
    
    //  @IBOutlet weak var titleView: MCTitleView!
    var viewTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.view.clipsToBounds = true
        
        //        if self.viewTitle != nil {
        //            self.titleView.title = self.viewTitle!
        //        }
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        let navBar = self.navigationController?.navigationBar as? MCNavigationBar
    //        let flowerButton: UIButton = UIButton(type: UIButtonType.custom)
    //        flowerButton.addTarget(self, action: #selector(MCBaseViewController.flowerButtonTap), for: UIControlEvents.touchUpInside)
    //        navBar!.flowerButton = flowerButton
    //
    //
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Navigation
    func flowerButtonTap() {
        
        var navigationController: UINavigationController?
        if self.tabBarController == nil {
            navigationController = self.navigationController!
        } else {
            navigationController = self.tabBarController?.parent as? UINavigationController
        }
        navigationController!.popToViewController(navigationController!.viewControllers[1], animated: true)
    }
    
    func openArticleViewer(_ articles: [MCArticle], selectedArticle: MCArticle) {
        self.openArticleViewer(articles, selectedArticle: selectedArticle, animated: true)
    }
    
    func openArticleViewer(_ articles: [MCArticle], selectedArticle: MCArticle, animated: Bool) {
        //        let articleViewer = self.storyboard?.instantiateViewController(withIdentifier: "MCArticleViewerController") as! MCArticleViewerController
        //        articleViewer.articlesArray = articles
        //        articleViewer.selectedArticle = selectedArticle
        //        self.navigationController?.pushViewController(articleViewer, animated: animated)
    }
    
    func paidAppButtonTap() {
        //        let url = URL(string: MCHelper.paidAppURL)
        //        UIApplication.shared.openURL(url!)
    }
    
    @objc func backButtonTap() {
        let navigationController = self.tabBarController?.parent as! UINavigationController
        navigationController.popToViewController(navigationController.viewControllers[1], animated: true)
    }
}
