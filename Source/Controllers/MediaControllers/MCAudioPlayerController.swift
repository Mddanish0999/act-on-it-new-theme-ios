//
//  MCAudioPlayerController.swift
//  Mindfulness
//
//  Created by saurabh tripathi on 10/09/2018
//  Copyright © 2015 Mindfull Creations. All rights reserved.
//

import UIKit
import AVFoundation
import Charts
import CircleProgressView
import MediaPlayer

class MCAudioPlayerController: MCBaseViewController {
    
    @IBOutlet weak var piechart: PieChartView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var mainSubView: UIView!
    
    @IBOutlet weak var circularProgressView: CircleProgressView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var lightblue: UIButton!
    @IBOutlet weak var red: UIButton!
    @IBOutlet weak var blue: UIButton!
    @IBOutlet weak var pink: UIButton!
    @IBOutlet weak var green: UIButton!
    @IBOutlet weak var yellow: UIButton!
    
    @IBOutlet weak var breadcrumbViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var breadcrumbView: UIView!
    
    @IBOutlet weak var rightHeaderButton: UIButton!
    @IBOutlet weak var rightHeaderButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightHeaderButtonTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var downloadInformView: UIView!
    @IBOutlet weak var downloadInformLabel: UILabel!
    
    var leadingConstraintConstant: CGFloat = 0.0
    var topConstraintConstant: CGFloat = 0.0
    var breadcrumbTitlesArray = [String]()
    
    static var red = 0.0
    static var blue = 0.0
    static var green = 0.0
    static var backcolor = " "
    
    var audioURL: URL?
    var mediaUrlString: String?
    
    var audioPlayer: AVPlayer!
    var playerItem: AVPlayerItem?
    let commandCenter = MPRemoteCommandCenter.shared()
    var nowPlayingInfo = [String: Any]()

    var downloadFlag = 0
    var informLabelTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupView()
        
        self.downloadInformLabel.textColor = .black
        self.downloadInformLabel.text = "Your file is downloading and buffering..."
        downloadInformLabel.font = UIFont(name: "BureauGroteskFBOneFive", size: 18)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MCAudioPlayerController.imageTapped(gesture:)))
        logoImage.addGestureRecognizer(tapGesture)
        logoImage.isUserInteractionEnabled = true
        piechart.transparentCircleColor = UIColor.clear
        
        MCMainMenuController.red = colorcode.red/255
        MCMainMenuController.green = colorcode.green/255
        MCMainMenuController.blue = colorcode.blue/255
        MCMainMenuController.backcolor = colorcode.imagesto
        
        DispatchQueue.main.async {
            self.view.backgroundColor = UIColor(red: CGFloat(MCMainMenuController.red),  green:  CGFloat(MCMainMenuController.green), blue: CGFloat(MCMainMenuController.blue), alpha: 1.0)
            
            self.logoImage.image = UIImage(named: MCMainMenuController.backcolor)
            self.breadcrumbTitlesArray = ListViewController.breadcrumptittle
        }
        
        pieChartUpdate()
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.circularProgressView.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.circularProgressView.addGestureRecognizer(swipeLeft)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        DispatchQueue.main.async {
            if LocalStorageManager.isLocalFileExistForUrl(self.mediaUrlString!) {
                self.downloadInformView.alpha = 0
            } else {
                if self.informLabelTimer == nil {
                    self.informLabelTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.handleDownloadInfoView), userInfo: nil, repeats: true)
                } else {
                    self.informLabelTimer.invalidate()
                    self.informLabelTimer = nil
                }
            }
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                try AVAudioSession.sharedInstance().setActive(true)
            } catch {
                // report for an error
            }
            self.startPlaying()
            self.setupCommandCenter()
            self.setupNowPlaying()
        }
    }
    
    private func setupView() {
        DispatchQueue.main.async {
            self.mainSubView.backgroundColor = .white
            //self.setGradientBackground(view: self.mainSubView)
        }
        rightHeaderButtonLeadingConstraint.constant =  self.view.bounds.width
        
        leadingConstraintConstant = 0.0
        topConstraintConstant = 0.0
        var heightConstraintConstant: CGFloat = 30.0
        let breadcrumbViewWidth = self.view.frame.width - 80
        
        for view in self.breadcrumbView.subviews {
            if view != rightHeaderButton {
                view.removeFromSuperview()
            }
        }
        
        var tag = 0
        for title in breadcrumbTitlesArray {
            let button = UIButton()
            button.tag = tag
            tag = tag + 1
            button.titleLabel?.font = rightHeaderButton.titleLabel?.font
            button.addTarget(self, action: #selector(self.breadcrumbButtonAction(_:)), for: .touchUpInside)
            
            button.setTitleColor(UIColor.white, for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle(" > \(title)", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            
            button.sizeToFit()
            
            breadcrumbView.addSubview(button)
            if leadingConstraintConstant + button.frame.width > breadcrumbViewWidth {
                
                leadingConstraintConstant = 0
                topConstraintConstant = topConstraintConstant + button.frame.height
                heightConstraintConstant = topConstraintConstant + button.frame.height
            }
            
            button.topAnchor.constraint(equalTo: breadcrumbView.topAnchor, constant: topConstraintConstant).isActive = true
            button.leadingAnchor.constraint(equalTo: breadcrumbView.leadingAnchor, constant: leadingConstraintConstant).isActive = true
            
            leadingConstraintConstant = leadingConstraintConstant + button.frame.width
        }
        
        rightHeaderButton.tag = tag
        breadcrumbViewHeightConstraint.constant = heightConstraintConstant
    }
    
    func pieChartUpdate() {
        let entry1 = PieChartDataEntry(value: 16.7, label: "")
        let entry2 = PieChartDataEntry(value: 16.7, label: "")
        let entry3 = PieChartDataEntry(value: 16.7, label: "")
        let entry4 = PieChartDataEntry(value: 16.7, label: "")
        let entry5 = PieChartDataEntry(value: 16.7, label: "")
        let entry6 = PieChartDataEntry(value: 16.7, label: "")
        let dataSet = PieChartDataSet(entries: [entry1, entry2, entry3, entry4, entry5, entry5], label: "")
        let data = PieChartData(dataSet: dataSet)
        piechart.data = data
        piechart.notifyDataSetChanged()
        piechart.legend.enabled = false
        dataSet.colors = [UIColor.yellow,UIColor.blue, UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0), UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0), UIColor.red, UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)]
        
        dataSet.valueColors = [UIColor.yellow,UIColor.blue, UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0), UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0),UIColor.red, UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)]
        piechart.minOffset = 0
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.right:
                    let moveForward: Float64 = 15
                    guard let duration = audioPlayer.currentItem?.duration else {
                        return
                    }
              
                    let playerCurrentTime = CMTimeGetSeconds(audioPlayer.currentTime())
                    let newTime = playerCurrentTime + moveForward
                    if newTime < CMTimeGetSeconds(duration) {
                    let time2: CMTime = CMTimeMake(Int64(newTime * 1000 as Float64), 1000)
                        audioPlayer.seek(to: time2)
                    }
                
                case UISwipeGestureRecognizerDirection.left:
                    let moveBackward: Float64 = 15
                    guard let duration = audioPlayer.currentItem?.duration else {
                        return
                    }
                     
                    let playerCurrentTime = CMTimeGetSeconds(audioPlayer.currentTime())
                    let newTime = playerCurrentTime - moveBackward
                    if newTime < CMTimeGetSeconds(duration) {
                        let time2: CMTime = CMTimeMake(Int64(newTime * 1000 as Float64), 1000)
                        audioPlayer.seek(to: time2)
                    }
                default:
                    break
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        audioPlayer?.pause()
        audioPlayer?.seek(to: kCMTimeZero)
        nowPlayingInfo.removeAll()
        playButton.isSelected = false
        
        NotificationCenter.default.removeObserver(self)
        commandCenter.playCommand.removeTarget(nil)
        commandCenter.pauseCommand.removeTarget(nil)
    }
    
    @objc func handleDownloadInfoView() {
        UIView.animate(withDuration: 2.0) {
            if self.downloadInformLabel.alpha == 0 {
                self.downloadInformLabel.alpha = 1
            } else {
                self.downloadInformLabel.alpha = 0
            }
        }
    }
    
    @IBAction func changecolor(_ sender: UIButton) {
        switch sender {
        case yellow:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 24/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 24
                colorcode.green = 212
                colorcode.imagesto = "yellow"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case blue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1.0)
                colorcode.red = 0
                colorcode.blue = 255
                colorcode.green = 0
                colorcode.imagesto = "blue"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
        
        case pink:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 245
                colorcode.green = 177
                colorcode.imagesto = "pink"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case green:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0)
                colorcode.red = 156
                colorcode.blue = 72
                colorcode.green = 222
                colorcode.imagesto = "lightgreen"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case red:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
                colorcode.red = 225
                colorcode.blue = 0
                colorcode.green = 0
                colorcode.imagesto = "red"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case lightblue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)
                colorcode.red = 150
                colorcode.blue = 255
                colorcode.green = 239
                colorcode.imagesto = "lightblue"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        default:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 240/255, blue: 255/255, alpha: 1.0)
                colorcode.imagesto = "lightblue"
            }
            return
        }
    }
    
    @IBAction func breadcrumbButtonAction(_ sender: UIButton) {
        if sender == rightHeaderButton {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[sender.tag + 3], animated: false)
            }
        } else {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[sender.tag + 3], animated: false)
            }
        }
    }
    
    // if logo touched then it goes to  home menu
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if (gesture.view as? UIImageView) != nil {
            if let viewControllers = self.navigationController?.viewControllers {
                self.navigationController?.popToViewController(viewControllers[2], animated: false)
            }
        }
    }
    
    func setupNowPlaying() {
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = playerItem?.currentTime().seconds
        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = playerItem?.asset.duration.seconds
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = audioPlayer?.rate

        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        if #available(iOS 13.0, *) {
            MPNowPlayingInfoCenter.default().playbackState = .playing
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func setupCommandCenter() {
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: "Act on it"]
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
        
        commandCenter.playCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            if self?.audioPlayer.rate == 0 && self?.playButton.isSelected == false {
                self?.audioPlayer.play()
                self?.playButton.isSelected = true
                self?.audioPlayer.rate = 1
            }
            return .success
        }
        commandCenter.pauseCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            if self?.audioPlayer.rate != 0 {
               self?.audioPlayer.pause()
               self?.playButton.isSelected = false
            }
            return .success
        }
    }
    
    // MARK: Audio Player Methods
    func startPlaying() {
        if let audioURL = audioURL {
            playerItem = AVPlayerItem(url: audioURL)
            audioPlayer = AVPlayer(playerItem: playerItem)
            let playerLayer = AVPlayerLayer(player: audioPlayer)
            playerLayer.frame = CGRect(x: 0, y: 0, width: 10, height: 50)
            
            self.view.layer.addSublayer(playerLayer)
            
            audioPlayer.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main) { (CMTime) -> Void in
                if self.audioPlayer.currentItem?.status == .readyToPlay {
                    self.updateProgress()
                }
            }
            
            self.circularProgressView.progress = 0
            audioPlayer.play()
            playButton.isSelected = true
        }
    }
    
    @IBAction func playandpause(_ sender: Any) {
        if audioPlayer?.rate == 0 {
            audioPlayer?.play()
            playButton.isSelected = true
        } else {
            audioPlayer?.pause()
            playButton.isSelected = false
        }
    }
    
    //  MARK: UI Updates
    @objc func updateProgress() {
        if self.informLabelTimer != nil && self.circularProgressView.progress == 100 {
            self.informLabelTimer.invalidate()
            self.informLabelTimer = nil
            self.downloadInformLabel.alpha = 0
        }
        
        if LocalStorageManager.isLocalFileExistForUrl(self.mediaUrlString!) {
            if self.downloadFlag == 0 {
                self.downloadInformLabel.text = "Your file has downloaded..."
                self.downloadInformLabel.alpha = 1
                self.downloadFlag = 1
                if self.informLabelTimer != nil {
                    UIView.animate(withDuration: 8.0) {
                        self.informLabelTimer.invalidate()
                        self.informLabelTimer = nil
                        self.downloadInformLabel.alpha = 0
                    }
                }
            }
        }
        
        if let playerItem = playerItem {
            let duration: Float64 = CMTimeGetSeconds(playerItem.asset.duration)
            let currentTime: Float64 = CMTimeGetSeconds(self.audioPlayer.currentTime())
            
            let newTime = currentTime/duration
            self.circularProgressView.setProgress(newTime, animated: true)
            
            if self.circularProgressView.progress == 100 {
                self.downloadInformView.isHidden = true
                self.downloadInformView.alpha = 0
            }
        }
    }
    
    @objc func playerDidFinishPlaying(_ notification: NSNotification) {
        playButton.isSelected = false
        self.circularProgressView.progress = 0
        audioPlayer.seek(to: kCMTimeZero)
    }
    
    @IBAction func crossButton(_ sender: Any) {
        self.navigationController?.popViewControllerWithEaseOut(animated: false)
    }
}
