//
//  MCMainMenuController.swift
//  Mindfulness
//
//  Created by localadmin on 8/4/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import Pastel

class MCMainMenuController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var lightblue: UIButton!
    @IBOutlet weak var red: UIButton!
    @IBOutlet weak var blue: UIButton!
    @IBOutlet weak var pink: UIButton!
    @IBOutlet weak var green: UIButton!
    @IBOutlet weak var yellow: UIButton!
    
    static var red = 0.0
    static var blue = 0.0
    static var green = 0.0
    static var backcolor = " "
    
    var mainMenues = MasterDataHandler.sharedInstance.getMainMenues()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paintSafeAreaBottomInset(withColor: .white)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            MCMainMenuController.red = colorcode.red/255
            MCMainMenuController.green = colorcode.green/255
            MCMainMenuController.blue = colorcode.blue/255
            MCMainMenuController.backcolor = colorcode.imagesto
            
            self.view.backgroundColor = UIColor(red: CGFloat(MCMainMenuController.red), green: CGFloat(MCMainMenuController.green), blue: CGFloat(MCMainMenuController.blue), alpha: 1.0)
            self.logoImage.image = UIImage(named: MCMainMenuController.backcolor)
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        let mainMenuController = self.storyboard?.instantiateViewController(withIdentifier: "graphViewController") as! graphViewController
        self.navigationController?.pushViewControllerWithEaseIn(mainMenuController, animated: false)
    }
    
    @IBAction func changecolor(_ sender: UIButton) {
        switch sender {

        case yellow:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 24/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 24
                colorcode.green = 212
                colorcode.imagesto = "yellow"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case blue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1.0)
                colorcode.red = 0
                colorcode.blue = 255
                colorcode.green = 0
                colorcode.imagesto = "blue"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
        
        case pink:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
                colorcode.red = 255
                colorcode.blue = 245
                colorcode.green = 177
                colorcode.imagesto = "pink"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case green:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0)
                colorcode.red = 156
                colorcode.blue = 72
                colorcode.green = 222
                colorcode.imagesto = "lightgreen"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case red:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
                colorcode.red = 225
                colorcode.blue = 0
                colorcode.green = 0
                colorcode.imagesto = "red"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        case lightblue:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 239/255, blue: 255/255, alpha: 1.0)
                colorcode.red = 150
                colorcode.blue = 255
                colorcode.green = 239
                colorcode.imagesto = "lightblue"
                self.logoImage.image = UIImage(named: colorcode.imagesto)
            }
            
        default:
            UIView.animate(withDuration: 0.8) {
                self.view.backgroundColor = UIColor(red: 150/255, green: 240/255, blue: 255/255, alpha: 1.0)
                colorcode.imagesto = "lightblue"
            }
            return
        }
    }
    
    @IBAction func topRightArrow(_ sender: Any) {
        let modalViewController = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "MoreViewController") as? MoreViewController
        let navContr = UINavigationController(rootViewController: modalViewController!)
        navContr.modalPresentationStyle = .custom  // << Mark this update
        navContr.modalPresentationStyle = .custom
        navContr.modalTransitionStyle = .crossDissolve

        present(navContr, animated: false, completion: nil)
    }
  
    private func showMenuViewController(menuDict: [String: Any]) {
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MCMenuViewController") as? MCMenuViewController {
            controller.menuDict = menuDict
            
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    private func showListView(menuDict: [String: Any]) {
        if let _ = menuDict["subItems"] as? [[String: Any]] {
            if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController {
                controller.menuDict = menuDict
                self.navigationController?.pushViewController(controller, animated: false)
            }
        }
    }
}


extension MCMainMenuController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainMenues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableViewCell", for: indexPath) as! menuTableViewCell
        let menuDict = mainMenues[indexPath.row]
        cell.nameLabel.text  = menuDict["Name"] as? String
        
        if indexPath.row == 0 {
            cell.uparrow.isHidden  = false
        } else {
            cell.uparrow.isHidden  = true
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuDict = mainMenues[indexPath.row]
        if let subItemsType = menuDict["subItemsType"] as? String {
            if subItemsType == "subMenu" {
                showMenuViewController(menuDict: mainMenues[indexPath.row])
            } else if subItemsType == "listMenu" {
                showListView(menuDict: mainMenues[indexPath.row])
            }
        }
    }
}
