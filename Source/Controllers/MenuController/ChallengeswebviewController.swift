////
////  ChallengeswebviewController.swift
////  Mindfulness
////
////  Created by localadmin on 4/6/19.
////  Copyright © 2019 Mindful Creations. All rights reserved.
////
//
//import UIKit
//import WebKit
//class ChallengeswebviewController: UIViewController,UIWebViewDelegate {
//
//    @IBOutlet weak var webiew: UIWebView!
//     @IBOutlet weak var logoImage: UIImageView!
//    @IBOutlet weak var Activity: UIActivityIndicatorView!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        let url = NSURL (string: "https://mindfulcreation.com")
//        
//     
//        let request = NSURLRequest(url: url! as URL)
//        webiew.loadRequest(request as URLRequest)
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MCMainMenuController.imageTapped(gesture:)))
//       
//            logoImage.addGestureRecognizer(tapGesture)
//            logoImage.isUserInteractionEnabled = true
//        
//        
//        
//        self.webiew.addSubview(self.Activity)
//        
//          let delay = 5 // seconds
//          self.Activity.startAnimating()
//         DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) {
//            self.Activity.stopAnimating()
//            self.Activity.isHidden = true
//        }
//        
//       // self.Activity.startAnimating()
//        self.Activity.color = .black
//        self.Activity.hidesWhenStopped = true
//  
//        
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    @IBAction func moreMenuButton(_ sender: UIButton) {
//        if let MoreViewController = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "MoreViewController") as? MoreViewController {
//            
//            self.navigationController?.pushViewController(MoreViewController, animated: false)
//        }
//        
//    }
//    
//    @objc func imageTapped(gesture: UIGestureRecognizer) {
//        
//        if (gesture.view as? UIImageView) != nil {
//            print("Image Tapped")
//            let mainMenuController = self.storyboard?.instantiateViewController(withIdentifier: "MCMainMenuController") as! MCMainMenuController
//            self.navigationController?.pushViewController(mainMenuController, animated: false)
//        }
//    }
//    
//    @IBAction func backButtonAction(_ sender: UIButton) {
//        
//        self.navigationController?.popViewController(animated: false)
//        
//    }
//}
//extension ChallengeswebviewController : WKNavigationDelegate {
//    
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
//        
//        let request = navigationAction.request
//        
//        if request.url?.host == "itunes.apple.com" {
//            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
//            decisionHandler(.allow)
//        }
//            
//        else {
//            decisionHandler(.allow)
//            return
//        }
//        decisionHandler(.cancel)
//        
//    }
//    
//    func webViewDidStartLoad(_ webView: UIWebView){
// 
//    }
//  
//    func webViewDidFinishLoad(_ webView: UIWebView){
//       
//    }
//    }
//    
//    
//    
//    
//
