//
//  menuTableViewCell.swift
//  Mindfulness
//
//  Created by localadmin on 8/2/19.
//  Copyright © 2019 Mindful Creations. All rights reserved.
//

import UIKit

class menuTableViewCell: UITableViewCell {

    @IBOutlet weak var uparrow: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.numberOfLines = 2
        nameLabel.textColor = .black
        nameLabel.font = UIFont.init(name: "BureauGroteskFBOneFive", size: 34)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(menuDict: [String: Any], indexPath: IndexPath) {
        if let name = menuDict["Name"] as? String {
            nameLabel.text = name
            nameLabel.sizeToFit()
        }
    }
}
