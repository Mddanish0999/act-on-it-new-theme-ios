//
//  graphViewController.swift
//  ACTONIT
//
//  Created by localadmin on 8/8/19.
//  Copyright © 2019 Mindful Creations. All rights reserved.
//

import UIKit
import Charts

class graphViewController: UIViewController {
    
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var piechart: PieChartView!
    
    @IBOutlet weak var lightblue: UIButton!
    @IBOutlet weak var red: UIButton!
    @IBOutlet weak var blue: UIButton!
    @IBOutlet weak var pink: UIButton!
    @IBOutlet weak var green: UIButton!
    @IBOutlet weak var yellow: UIButton!
    
    @IBOutlet weak var changeForColorMsgView: UIView!
    @IBOutlet weak var changeForColorMsgLabel: UILabel!
    
    @IBOutlet weak var moodLabel: UILabel!
    @IBOutlet weak var questionMarkView: UIView!
    @IBOutlet weak var questionMarkButton: UIButton!
    
    static var sau: UIColor?
    var informLabelTimer: Timer!
    
    @IBAction func changecolor(_ sender: UIButton) {
        switch sender {
    
        case yellow:
            colorcode.red = 255
            colorcode.blue = 24
            colorcode.green = 212
            colorcode.imagesto = "yellow"
        
        case blue:
            colorcode.blue = 255
            colorcode.green = 0
            colorcode.red = 0
            colorcode.imagesto = "blue"
        
        case pink:
            colorcode.red = 255
            colorcode.blue = 245
            colorcode.green = 177
            colorcode.imagesto = "pink"
         
        case green:
            colorcode.red = 156
            colorcode.blue = 72
            colorcode.green = 222
            colorcode.imagesto = "lightgreen"
           
        case red:
            colorcode.red = 225
            colorcode.blue = 28
            colorcode.green = 62
            colorcode.imagesto = "red"

        case lightblue:
            colorcode.red = 150
            colorcode.blue = 255
            colorcode.green = 239
            colorcode.imagesto = "lightblue"
            
        default:
            self.view.backgroundColor = UIColor(red: 150/255, green: 240/255, blue: 255/255, alpha: 1.0)
            colorcode.imagesto = "lightblue"
            return
        }
        
        let mainMenuController = self.storyboard?.instantiateViewController(withIdentifier: "MCMainMenuController") as! MCMainMenuController
        self.navigationController?.pushViewControllerWithEaseIn(mainMenuController, animated: false)
        
    }
    
    var yellow1 = UIColor(red: 255/255, green: 212/255, blue: 24/255, alpha: 1.0)
    var pink1 = UIColor(red: 255/255, green: 177/255, blue: 245/255, alpha: 1.0)
    var green1 = UIColor(red: 156/255, green: 222/255, blue: 72/255, alpha: 1.0)
    var lightblue1 = UIColor(red: 150/255, green: 240/255, blue: 255/255, alpha: 1.0)
    var blue1 = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1.0)
    var colorCodes = colorcode()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pieChartUpdate()
        pieChartUpdateinner()
        pieChart.delegate = self
        piechart.delegate = self
        
        changeForColorMsgView.backgroundColor = .clear
        
        changeForColorMsgLabel.textColor = .black
        questionMarkButton.setTitleColor(.black, for: .normal)
        changeForColorMsgLabel.font = UIFont.init(name: "BureauGroteskFBOneFive", size: 28)
        changeForColorMsgLabel.textAlignment = .center
        changeForColorMsgLabel.numberOfLines = 0
        changeForColorMsgLabel.text = "Choose a color to suit how you're feeling?"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        questionMarkView.isUserInteractionEnabled = true
        moodLabel.isUserInteractionEnabled = true
        let colorChangeGesture = UITapGestureRecognizer(target: self, action: #selector(changeColorMsg))
        moodLabel.addGestureRecognizer(colorChangeGesture)
        questionMarkButton.addTarget(self, action: #selector(changeColorMsg), for: .touchUpInside)
    }
    
    func pieChartUpdate() {
        let entry1 = PieChartDataEntry(value: 16.712, label: "")
        let entry2 = PieChartDataEntry(value: 16.712, label: "")
        let entry3 = PieChartDataEntry(value: 16.712, label: "")
        let entry4 = PieChartDataEntry(value: 16.712, label: "")
        let entry5 = PieChartDataEntry(value: 16.712, label: "")
        let entry6 = PieChartDataEntry(value: 16.712, label: "")
        
        pieChart.transparentCircleColor = UIColor.clear
        
        let dataSet = PieChartDataSet(entries: [entry1, entry2, entry3, entry4, entry5, entry6], label:"")
        let data = PieChartData(dataSet:dataSet)
        pieChart.data = data
        pieChart.notifyDataSetChanged()
        pieChart.legend.enabled = false
        
        dataSet.colors = [blue1, pink1, green1, UIColor.red, lightblue1, yellow1]
        dataSet.valueColors = [blue1, pink1, green1, UIColor.red, lightblue1, yellow1]
    }
    
    @objc func changeColorMsg(gesture: Any) {
        if self.informLabelTimer == nil {
            self.informLabelTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleColorMsgView), userInfo: nil, repeats: true)
        } else {
            self.informLabelTimer.invalidate()
            self.informLabelTimer = nil
        }
    }
    
    @objc private func handleColorMsgView() {
        UIView.animate(withDuration: 8.0) {
            if self.changeForColorMsgView.alpha == 0 {
                self.changeForColorMsgView.alpha = 1
            } else {
                self.changeForColorMsgView.alpha = 0
                self.informLabelTimer.invalidate()
                self.informLabelTimer = nil
            }
        }
    }
    
    func pieChartUpdateinner() {
        let entry1 = PieChartDataEntry(value: 16.72, label: "")
        let entry2 = PieChartDataEntry(value: 16.72, label: "")
        let entry3 = PieChartDataEntry(value: 16.72, label: "")
        let entry4 = PieChartDataEntry(value: 16.72, label: "")
        let entry5 = PieChartDataEntry(value: 16.72, label: "")
        let entry6 = PieChartDataEntry(value: 16.72, label: "")
        
        piechart.transparentCircleColor = UIColor.clear
        
        let dataSet = PieChartDataSet(entries: [entry1, entry2, entry3, entry4, entry5, entry6], label:"")
        let data = PieChartData(dataSet:dataSet)
        piechart.data = data
        piechart.notifyDataSetChanged()
        piechart.legend.enabled = false
    
        dataSet.colors = [UIColor.red, lightblue1, yellow1, blue1, pink1, green1]
        dataSet.valueColors = [UIColor.red, lightblue1, yellow1, blue1, pink1, green1]
    }
}

extension graphViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let dataSet = chartView.data?.dataSets[ highlight.dataSetIndex] {
            let sliceIndex: Int = dataSet.entryIndex( entry: entry)
    
            graphViewController.sau = dataSet.valueColors[sliceIndex]
   
            switch graphViewController.sau {
                
            case yellow1:
                colorcode.red = 255
                colorcode.blue = 24
                colorcode.green = 212
                colorcode.imagesto = "yellow"

            case blue1:
                colorcode.red = 0
                colorcode.blue = 255
                colorcode.green = 0
                colorcode.imagesto = "blue"
            
            case pink1:
                colorcode.red = 255
                colorcode.blue = 245
                colorcode.green = 177
                colorcode.imagesto = "pink"
             
            case green1:
                colorcode.red = 156
                colorcode.blue = 72
                colorcode.green = 222
                colorcode.imagesto = "lightgreen"
               
            case UIColor.red:
                colorcode.red = 255
                colorcode.blue = 28
                colorcode.green = 59
                colorcode.imagesto = "red"
             
            case lightblue1:
                colorcode.red = 150
                colorcode.blue = 255
                colorcode.green = 239
                colorcode.imagesto = "lightblue"
               
            default:
                colorcode.imagesto = "lightblue"
                return
            }
            
            chartView.highlightValues(nil)
            let mainMenuController = self.storyboard?.instantiateViewController(withIdentifier: "MCMainMenuController") as! MCMainMenuController

            self.navigationController?.pushViewControllerWithEaseIn(mainMenuController, animated: false)
        }
    }
}
