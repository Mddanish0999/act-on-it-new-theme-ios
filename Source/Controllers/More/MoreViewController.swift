//
//  MoreViewController.swift
//  Mindfulness
//
//  Created by Rajinder on 10/6/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {
    
    @IBOutlet weak var moreTableView: UITableView!
    
    var dataArray = MasterDataHandler.sharedInstance.getHeaderMenues()!
    var colorCodes = colorcode()
    
    static var red = 0.0
    static var blue = 0.0
    static var green = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.isOpaque = false
        
        view.backgroundColor = UIColor(red: 242/255, green: 235/255, blue: 228/255, alpha: 0.978)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        moreTableView.estimatedRowHeight = 40
        moreTableView.rowHeight = UITableViewAutomaticDimension
        
        moreTableView.estimatedSectionHeaderHeight = 30
        moreTableView.sectionHeaderHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MoreViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if let subItems = dataArray[section]["subItems"] as? [[String: String]] {
            rows = subItems.count
        }
        return rows
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        headerLabel.text = dataArray[section]["Name"] as? String
        headerLabel.font = UIFont(name: "roboto-regular", size: 18)
        headerLabel.textColor = .black
        return headerLabel
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell", for: indexPath) as? MoreTableViewCell else {
            fatalError()
        }
        
        if let subItems = dataArray[indexPath.section]["subItems"] as? [[String: String]] {
            cell.mameLabel.text = subItems[indexPath.row]["Name"]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = dataArray[indexPath.section]["Name"] as? String
        
        switch sectionName {
            
        case "DOWNLOADS":
            showManageStorageController()
            
            
        default:
            if let subItems = dataArray[indexPath.section]["subItems"] as? [[String: String]] {
              
                let mediaType = subItems[indexPath.row]["mediaType"]
                if mediaType == "html" {
                    if let  controller = UIStoryboard(name: "Media", bundle: nil).instantiateViewController(withIdentifier: "MCWebViewController") as? MCWebViewController {
                        if let htmlName = subItems[indexPath.row]["htmlName"] as? String {
                            controller.htmlName = htmlName
                            self.navigationController?.pushViewController(controller, animated: false)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showManageStorageController() {
        if let ManageStorageViewController = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "ManageStorageViewController") as? ManageStorageViewController {
            
            self.navigationController?.pushViewController(ManageStorageViewController, animated: false)
        }
    }
}
