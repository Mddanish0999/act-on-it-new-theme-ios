//
//  ManageStorageViewController.swift
//  Mindfulness
//
//  Created by apple on 07/10/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit

class ManageStorageViewController: UIViewController {

    @IBOutlet weak var SelectAll: UIButton!
    @IBOutlet weak var downloadsTableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!

    @IBOutlet weak var DeleteButton: UIButton!
    
    var downloadedFiles: [[String: Any]]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupData() {
        noDataLabel.textColor = .black
        downloadedFiles = LocalStorageManager.getDownloadedFiles()
        
        if let downloadedFiles = downloadedFiles {
            if downloadedFiles.count > 0 {
                noDataLabel.isHidden = true
            }
        }
        if let downloadedFiles = downloadedFiles {
            if downloadedFiles.count == 0 {
                SelectAll.isHidden = true
                DeleteButton.isHidden = true
            }
        }
    }
}

extension ManageStorageViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if let downloadedFiles = downloadedFiles {
            rows = downloadedFiles.count
        }
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ManageStorageTableViewCell", for: indexPath) as? ManageStorageTableViewCell else {
            fatalError()
        }
        
        if let downloadedFiles = downloadedFiles {
            if let name = downloadedFiles[indexPath.row]["name"] as? String {
                cell.mameLabel.text = name
            }
            
            if let size = downloadedFiles[indexPath.row]["size"] as? Double {
                cell.sizeLabel.text = String(format: " (%.2f MB)", size)
            } else {
                cell.sizeLabel.text = ""
            }
        }
        return cell
    }
    
    @IBAction func selectAllButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if let downloadedFiles = downloadedFiles {
            for index in 0..<downloadedFiles.count {
                if sender.isSelected {
                    downloadsTableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
                } else {
                    downloadsTableView.deselectRow(at: IndexPath(row: index, section: 0), animated: false)
                }
            }
        }
    }

    @IBAction func deleteButtonAction(_ sender: UIButton) {
        if let selectedIndexPaths = downloadsTableView.indexPathsForSelectedRows {
            let deleteAlert = UIAlertController(title: nil, message: "These files will be deleted from device and you wont be able to play them in offline.", preferredStyle: .actionSheet)
            
            let deleteAction = UIAlertAction(title: "Delete \(selectedIndexPaths.count) files.", style: .destructive) { (action) in
                //remove files
                self.deleteFilesAtIndexPaths(indexPaths: selectedIndexPaths)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            deleteAlert.addAction(deleteAction)
            deleteAlert.addAction(cancelAction)
            
            self.present(deleteAlert, animated: false, completion: nil)
        }
    }

    func deleteFilesAtIndexPaths(indexPaths: [IndexPath]) {
        var files = [String]()
        for indexPath in indexPaths {
            if let name = self.downloadedFiles?[indexPath.row]["name"] as? String {
                files.append(name)
            }
        }
        
        LocalStorageManager.removeDownloadedFiles(files: files)
        setupData()
        downloadsTableView.reloadData()

    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}
