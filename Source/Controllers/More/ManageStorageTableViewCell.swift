//
//  ManageStorageTableViewCell.swift
//  Mindfulness
//
//  Created by Rajinder on 10/6/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit

class ManageStorageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mameLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        mameLabel.textColor = .black
        sizeLabel.textColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        checkButton.isSelected = selected
    }
}
