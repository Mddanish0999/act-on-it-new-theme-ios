//
//  MoreTableViewCell.swift
//  Mindfulness
//
//  Created by Rajinder on 10/6/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {
    @IBOutlet weak var mameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mameLabel.numberOfLines = 2
        mameLabel.textColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            mameLabel.textColor = UIColor.white
        } else {
            mameLabel.textColor = UIColor.black
        }
    }
}
