//
//  Quotes.swift
//  Mindfulness
//
//  Created by Dinesh Raja on 9/30/15.
//  Copyright © 2015 Mindful Creations. All rights reserved.
//

import Foundation

struct Quote {
    var quoteText: String?
    var author: String?
}

class Quotes {
    static var allQuotes = [Quote]()
    
    class func randomQuote() -> Quote {
        if Quotes.allQuotes.count == 0 {
            let filePath = Bundle.main.path(forResource: "quotes", ofType: "txt")
            do {
                var quoteObjects = [Quote]()
                let quoteCSVstring = try NSString(contentsOfFile: filePath!, encoding: String.Encoding.utf8.rawValue)
                let quoteArray = quoteCSVstring.components(separatedBy: NSCharacterSet.newlines)
                for quote in quoteArray {
                    let quoteComponents = quote.components(separatedBy: "|")
                    
                    var author = ""
                    if quoteComponents.count > 1 {
                        author = quoteComponents[1]
                    }
                    
                    let newQuote: Quote = Quote(quoteText: quoteComponents[0], author: author)
                    quoteObjects.append(newQuote)
                }
                Quotes.allQuotes = quoteObjects
            } catch {
                print("Can't find content of quotes file")
            }
        }
        
        return Quotes.allQuotes[Int(arc4random_uniform(UInt32(Quotes.allQuotes.count)))]
    }
}
