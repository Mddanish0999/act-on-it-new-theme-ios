//
//  MCMainMenuController.swift
//  Mindfulness
//
//  Created by localadmin on 8/4/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//

import UIKit
import Foundation

class MCInitialViewController: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    @IBOutlet weak var thirdLabel: UILabel!
    
    @IBOutlet weak var fourthLabel: UILabel!
    
    @IBOutlet weak var fifthLabel: UILabel!
    
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            self.gotoMainMenuController()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    func gotoMainMenuController() {
        let mainMenuController = self.storyboard?.instantiateViewController(withIdentifier: "afterQuoteViewController") as! afterQuoteViewController

        let animation = CATransition()
        animation.type = kCATransitionFade
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        
        self.navigationController?.view.layer.add(animation, forKey: "FadeTransition")
        self.navigationController?.pushViewController(mainMenuController, animated: false)
    }
}
