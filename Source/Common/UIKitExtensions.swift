//
//  UIKitExtensions.swift
//  Mindfulness
//
//  Created by localadmin on 8/2/18.
//  Copyright © 2018 Mindful Creations. All rights reserved.
//
import UIKit
import Pastel

//MARK:- UIViewController Extensions
extension UIViewController {
    func setGradientBackground(view: UIView) {
        let pastelView: PastelView = PastelView(frame:view.bounds)
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        //MARK: -  Custom Duration
        
        pastelView.animationDuration = 6
        
        //MARK: -  Custom Color
        
        pastelView.setColors([UIColor(red: 222/255, green: 188/255, blue: 196/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 163/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 163/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 0.5373,  green: 0.9569,  blue: 0.8118,  alpha: 1.0),
                              UIColor(red: 138/255, green: 232/255, blue: 214/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 193/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 138/255, green: 232/255, blue: 214/255, alpha: 1.0),
                              UIColor(red: 236/255, green: 193/255, blue: 155/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 166/255, green: 222/255, blue: 197/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 138/255, green: 232/255, blue: 214/255, alpha: 1.0),
                              UIColor(red: 236/255, green: 193/255, blue: 155/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 163/255, alpha: 1.0),
                              UIColor(red: 247/255, green: 188/255, blue: 146/255, alpha: 1.0),
                              UIColor(red: 222/255, green: 200/255, blue: 163/255, alpha: 1.0),
                              UIColor(red: 153/255, green: 255/255, blue: 230/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
    }
    
    func startPastelAnimation(view: UIView) {
        if let pastelView = view.subviews[0] as? PastelView {
            pastelView.startAnimation()
        }
    }
}

extension UIViewController {
    func paintSafeAreaBottomInset(withColor color: UIColor) {
        guard #available(iOS 11.0, *) else {
            return
        }
        
        let insetView = UIView(frame: .zero)
        
        insetView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(insetView)
        view.sendSubview(toBack: insetView)
        
        insetView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        insetView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        insetView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        insetView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        insetView.backgroundColor = color
    }
}

extension UINavigationController {
    func pushViewControllerWithEaseIn(_ viewController: UIViewController, animated: Bool) {
        let animationSequence = CPAnimationSequence.sequenceWithSteps(
            CPAnimationStep.animateFor(0, animation: {
                let animation = CATransition()
                animation.type = kCATransitionFade
                animation.duration = 0.8
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
                
                self.view.layer.add(animation, forKey: "FadeTransition")
                self.pushViewController(viewController, animated: animated)
            }))
        animationSequence.run()
    }
    
    func popViewControllerWithEaseOut(animated: Bool) {
        let animationSequence = CPAnimationSequence.sequenceWithSteps(
            CPAnimationStep.animateFor(0, animation: {
                let animation = CATransition()
                animation.type = kCATransitionFade
                animation.duration = 0.8
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
                
                self.view.layer.add(animation, forKey: "FadeTransition")
                self.popViewController(animated: animated)
                
            }))
        animationSequence.run()
    }
}
